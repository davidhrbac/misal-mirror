﻿function UvodniNastaveni()
{
      zacatek = PrevedNaDatum(29,11,2020);
      zacatek_rok = zacatek.getFullYear();
      konec = PrevedNaDatum(27,11,2021);
      konec_rok = konec.getFullYear();
      dnes = new Date();
      dnes.setHours(12);
//      dnes = PrevedNaDatum(24,2,2017);
      celkem_dnu = RozdilDnu(konec,zacatek);
      pro_pripominku = new Array();
      den_mezi_1 = zacatek;
      /*den_mezi_2 = PrevedNaDatum(21,11,2020);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle v mezidobí"," ","týdne v mezidobí",33,"06mezidobi/",6,12,"Z","","Gloria, Krédo, preface pro neděle v mezidobí","","","",false);
      den_mezi_1 = PrevedNaDatum(22,11,2020);
      den_mezi_2 = PrevedNaDatum(28,11,2020);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle v mezidobí"," ","týdne v mezidobí",34,"06mezidobi/",6,12,"Z","","Gloria, Krédo, preface pro neděle v mezidobí","","","",false);
      den_mezi_1 = PrevedNaDatum(29,11,2020);*/
      den_mezi_2 = PrevedNaDatum(16,12,2020);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle adventní","po","neděli adventní",1,"01advent/",2,13,"F","","Gloria se neříká, Krédo, preface 1. adventní","preface 1. adventní","v modlitbě se čtením chvalozpěv <i>Te Deum</i>","",false);
      den_mezi_1 = PrevedNaDatum(17,12,2020);
      den_mezi_2 = PrevedNaDatum(24,12,2020);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle adventní","","",3,"01advent/",2,9,"F","prosince","Gloria se neříká, Krédo, preface 2. adventní","preface 2. adventní","připadne-li na 24. prosince, berou se všechny texty z tohoto dne","",true);
      den_mezi_1 = PrevedNaDatum(25,12,2020);
      den_mezi_2 = PrevedNaDatum(31,12,2020);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle po Narození Páně","","",0,"02vanoce/",6,9,"B","prosince","Gloria, Krédo, preface o Narození Páně, v římském kánonu <i>V tomto společenství</i> ze slavnostni Narození Páně","Gloria, preface o Narození Páně, v římském kánonu <i>V tomto společenství</i> ze slavnostni Narození Páně","","V modlitbě se čtením vánoční hymnus a vše ostatní vlastní, na závěr chvalozpěv <i>Te Deum</i>. V ranních chválách žalmy nedělní z 1. týdne. V modlitbě uprostřed žalmy z příslušného dne v týdnu. V nešporách hymnus, antifony, žalmy a kantikum jako ve 2. nešporách Narození Páně, krátké čtení s příslušným zpěvem, prosby a závěrečná modlitba vlastní. Modlitba před spaním jako po 1. nebo 2. nedělních nešporách.",true);//XXX
      den_mezi_1 = PrevedNaDatum(1,1,2021);
      den_mezi_2 = PrevedNaDatum(3,1,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle po Narození Páně","","",1,"02vanoce/",6,13,"B","ledna","Gloria, Krédo, preface o Narození Páně","preface o Narození Páně","vše vlastní, má první nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",true);
      den_mezi_1 = PrevedNaDatum(4,1,2021);
      den_mezi_2 = PrevedNaDatum(5,1,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle po Narození Páně","","",1,"02vanoce/",6,13,"B","ledna","Gloria, Krédo, preface o Narození Páně (nebo po slavnosti Zjevení Páně: o Zjevení Páně nebo o Narození Páně)","preface o Narození Páně (nebo po slavnosti Zjevení Páně: o Zjevení Páně nebo o Narození Páně)","vše vlastní, má první nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",true);
      den_mezi_1 = PrevedNaDatum(6,1,2021);
      den_mezi_2 = PrevedNaDatum(10,1,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle po Narození Páně","","",2,"02vanoce/",6,13,"B","ledna","Gloria, Krédo, preface o Narození Páně (nebo po slavnosti Zjevení Páně: o Zjevení Páně nebo o Narození Páně)","preface o Narození Páně (nebo po slavnosti Zjevení Páně: o Zjevení Páně nebo o Narození Páně)","vše vlastní, má první nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",true);
      den_mezi_1 = PrevedNaDatum(11,1,2021);
      den_mezi_2 = PrevedNaDatum(16,2,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle v mezidobí"," ","týdne v mezidobí",1,"06mezidobi/",6,12,"Z","","Gloria, Krédo, preface pro neděle v mezidobí","preface obecná","","",false);
      den_mezi_1 = PrevedNaDatum(17,2,2021);
      den_mezi_2 = PrevedNaDatum(20,2,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"","","po Popeleční středě",0,"03pust/",2,9,"F","","Gloria se neříká, Krédo, preface 1. nebo 2. postní, na konci modlitba nad lidem","preface postní (zvláště 3. nebo 4.), na konci se může vložit modlitba nad lidem","","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",false);
      den_mezi_1 = PrevedNaDatum(21,2,2021);
      den_mezi_2 = PrevedNaDatum(20,3,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle postní","po","neděli postní",1,"03pust/",2,9,"F","","Gloria se neříká, Krédo, preface 1. nebo 2. postní, na konci modlitba nad lidem","preface postní, zvláště 3. nebo 4., na konci se může vložit modlitba nad lidem","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",false);
      den_mezi_1 = PrevedNaDatum(21,3,2021);
      den_mezi_2 = PrevedNaDatum(27,3,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle postní","po","neděli postní",5,"03pust/",2,9,"F","","Gloria se neříká, Krédo, preface 1. o utrpení Páně, na konci modlitba nad lidem","preface 1. o utrpení Páně, na konci se může vložit modlitba nad lidem","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>","krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní",false);
      den_mezi_1 = PrevedNaDatum(28,3,2021);
      den_mezi_2 = PrevedNaDatum(1,4,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"Květná neděle","","Svatého týdne",6,"03pust/",2,2,"F","","Gloria se neříká, Krédo, preface 2. o utrpení Páně, na konci modlitba nad lidem","preface 2. o utrpení Páně, na konci se může vložit modlitba nad lidem","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, prosby a závěrečná modlitba vlastní",false);
      den_mezi_1 = PrevedNaDatum(2,4,2021);
      den_mezi_2 = PrevedNaDatum(4,4,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"","","Triduum",0,"04triduum/",1,1,"B","","","","","",false);
      den_mezi_1 = PrevedNaDatum(5,4,2021);
      den_mezi_2 = PrevedNaDatum(10,4,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"Slavnost Zmrtvýchvstání Páně","","v oktávu velikonočním",1,"05velikonoce/",2,2,"B","","jako úkon kajícnosti je vhodné kropení lidu vodou posvěcenou při vigilii, Gloria, sekvence, Krédo, preface 1. velikonoční (<i>zvláště nyní o Velikonocích</i>), v římském kánonu vlastní <i>V tomto společenství...</i> a <i>Přijmi tedy</i>, nelze použít 4. anafory, na závěr se mohou žehnat pokrmy, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>, večerní mše má vlastní evangelium","Gloria, lze vložit sekvenci, preface 1. velikonoční (<i>zvláště nyní o Velikonocích</i>), v římském kánonu vlastní <i>V tomto společenství...</i> a <i>Přijmi tedy</i>, nelze použít 4. anafory, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>","","V modlitbě se čtením a během dne hymnus velikonoční, ostatní vlastní, v modlitbě se čtením na závěr <i>Te Deum</i>. V ranních chválách a nešporách antifony, žalmy a kantikum jako v neděli Zmrtvýchvstání Páně, krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. V kompletáři vlastní zpěv po krátkém čtení, ostatní jako v neděli po 1. nebo 2. nešporách.",false);
      den_mezi_1 = PrevedNaDatum(11,4,2021);
      den_mezi_2 = PrevedNaDatum(12,5,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle velikonoční","po","neděli velikonoční",2,"05velikonoce/",2,13,"B","","Gloria, Krédo, preface velikonoční","preface velikonoční","V nešporách a ranních chválách antifony, krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. V modlitbě se čtením <i>Te Deum</i>. V modlitbě během dne krátké čtení se zpěvem vlastní.","V ranních chválách a nešporách krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. V modlitbě během dne krátké čtení se zpěvem vlastní.",false);
      den_mezi_1 = PrevedNaDatum(13,5,2021);
      den_mezi_2 = PrevedNaDatum(23,5,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle velikonoční","po","neděli velikonoční",6,"05velikonoce/",2,13,"B","","Gloria, Krédo, preface o Nanebestoupení Páně nebo velikonoční","preface o Nanebevstoupení Páně nebo velikonoční","V nešporách a ranních chválách antifony, krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. V modlitbě se čtením <i>Te Deum</i>. V modlitbě během dne krátké čtení se zpěvem vlastní.","V ranních chválách a nešporách krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. V modlitbě během dne krátké čtení se zpěvem vlastní.",false);
      den_mezi_1 = PrevedNaDatum(24,5,2021);
      den_mezi_2 = PrevedNaDatum(20,11,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle v mezidobí"," ","týdne v mezidobí",8,"06mezidobi/",6,12,"Z","","Gloria, Krédo, preface pro neděle v mezidobí","","","",false);
      den_mezi_1 = PrevedNaDatum(21,11,2021);
      den_mezi_2 = PrevedNaDatum(27,11,2021);
      GenerovatPravidelne(den_mezi_1.getDate(),den_mezi_1.getMonth()+1,den_mezi_1.getFullYear(),den_mezi_2.getDate(),den_mezi_2.getMonth()+1,den_mezi_2.getFullYear(),"neděle v mezidobí"," ","týdne v mezidobí",34,"06mezidobi/",6,12,"Z","","Gloria, Krédo, preface pro neděle v mezidobí","","","hymny z 34. týdne v mezidobí",false);
      GenerovatSanctoral();
      GenerovatVyjimky();
      
}




var DenVTydnu = new Array("neděle","pondělí","úterý","středa","čtvrtek","pátek","sobota");
var DenVTydnu_zkr = new Array("NE","PO","ÚT","ST","ČT","PÁ","SO");
var Mesic = new Array("ledna","února","března","dubna","května","června","července","srpna","září","října","listopadu","prosince");
var Mesic_1p = new Array("leden","únor","březen","duben","květen","červen","červenec","srpen","září","říjen","listopad","prosinec");
var co_se_slavi = new Array();
var pravidelne = new Array();
var sanctoral = new Array();
var vyjimky = new Array();




function PrevedNaDatum(den, mesic,rok)
{
	var datum = new Date();
	datum.setFullYear(rok);
	datum.setHours(12);
	datum.setDate(15);
	datum.setMonth(mesic - 1);
	datum.setDate(den);
	return datum;
}

function DalsiDen(aktualni)
{
	var hodina = aktualni.getHours();
	aktualni.setTime(aktualni.getTime() + 24*60*60*1000);
	aktualni.setHours(hodina);
	return aktualni;
}


function RozdilDnu(A,B)
{
	rozdil = Math.round((A-B) / (24*60*60*1000));
	return rozdil;
}

function Barva(barva)
{
	var color = barva.toUpperCase();
	switch (barva){
		case "Z": color="green"; break;
		case "B": color="black"; break;
		case "F": color="violet"; break;
		case "C": color="darkred"; break;
		case "Č": color="darkred"; break;
		case "R": color="pink"; break;
		default: color = "black"
	}
	return color;
}

function BarvaSlovy(barva)
{
	var color = barva.toUpperCase();
	switch (barva){
		case "Z": color="zelená"; break;
		case "B": color="bílá"; break;
		case "F": color="fialová"; break;
		case "C": color="červená"; break;
		case "Č": color="červená"; break;
		case "R": color="růžová"; break;
		default: color = "???"
	}
	return color;
}

function Typ(typ)
{
	var vysledek = typ.toUpperCase();
	switch (vysledek){
		case "S": vysledek="Slavnost"; break;
		case "SV": vysledek="Svátek"; break;
		case "P": vysledek="Památka"; break;
		case "NP": vysledek="Nezávazná památka"; break;
		case "PP": vysledek="Pro připomínku"; break;
		case "F": vysledek="Feriální den"; break;
		case "N": vysledek="Neděle"; break;
		default: vysledek = typ
	}
	return vysledek;
}

function TentoTyp(typ)
{
	var vysledek = typ.toUpperCase();
	switch (vysledek){
		case "S": vysledek="Tato slavnost"; break;
		case "SV": vysledek="Tento svátek"; break;
		case "P": vysledek="Tato památka"; break;
		case "NP": vysledek="Tato nezávazná památka"; break;
		case "PP": vysledek="Tato památka pro připomínku"; break;
		case "F": vysledek="Tento feriální den"; break;
		case "N": vysledek="Tato neděle"; break;
		default: vysledek = typ
	}
	return vysledek;
}

function DveCislice(cislo)
{
	if ((cislo>=0) && (cislo<=9))
	{
		var vysledek = "0"+cislo;
	}
	else
	{
		var vysledek = cislo;
	}
	return vysledek;
}

function GenerovatPravidelne(od_den, od_mesic,od_rok,do_den, do_mesic,do_rok,popis_nedele,popis_ferie_1,popis_ferie_2,kolikaty_tyden,adresar,tabulka_lit_dnu_nedele,tabulka_lit_dnu_ferie,barva,mesic,ke_msi_nedele,ke_msi_ferie,k_liturgii_hodin_nedele,k_liturgii_hodin_ferie,vypsat_datum)
{
	var datum_od = PrevedNaDatum(od_den, od_mesic,od_rok);
	var datum_do = PrevedNaDatum(do_den, do_mesic,do_rok);
	//od.setMonth(2);
	var aktualni = datum_od;
	var i = pravidelne.length;
	var aktualni_tyden = kolikaty_tyden;
	if (aktualni.getDay() == 0)
	{
		aktualni_tyden = aktualni_tyden - 1;
	}
	while (aktualni <= datum_do)
	{
		pravidelne[i] = new Array();
		co_se_slavi[i] = new Array();
		//pravidelne[i]["datum"] = PrevedNaDatum(aktualni.getDate(),aktualni.getMonth()+1,aktualni.getFullYear());
		if (aktualni.getDay() == 0)
		{
			aktualni_tyden = aktualni_tyden + 1;
			pravidelne[i]["nazev"] = aktualni_tyden+". "+popis_nedele;
			pravidelne[i]["tabulka_lit_dnu"] = tabulka_lit_dnu_nedele;
			pravidelne[i]["ke_msi"] = ke_msi_nedele;
			pravidelne[i]["k_liturgii_hodin"] = k_liturgii_hodin_nedele;
		}
		else
		{
			if (mesic)
			{
				pravidelne[i]["nazev"] = aktualni.getDate()+". "+mesic;
			}
			else
			{
				pravidelne[i]["nazev"] = DenVTydnu[aktualni.getDay()]+" "+popis_ferie_1+" ";
				if (popis_ferie_1 != "")
				{
				  pravidelne[i]["nazev"] += aktualni_tyden+". ";
				}
				pravidelne[i]["nazev"] += popis_ferie_2;
			}
			pravidelne[i]["tabulka_lit_dnu"] = tabulka_lit_dnu_ferie;
			pravidelne[i]["ke_msi"] = ke_msi_ferie;
			pravidelne[i]["k_liturgii_hodin"] = k_liturgii_hodin_ferie;
			//pravidelne[i]["ke_msi"] = "feriální";
		}
		if ((vypsat_datum) && (aktualni.getDay() != 0))
		{
			pravidelne[i]["odkaz"] = adresar+DveCislice(aktualni.getMonth()+1)+"_"+DveCislice(aktualni.getDate())+".htm";
		}
		else
		{
			pravidelne[i]["odkaz"] = adresar+DveCislice(aktualni_tyden)+"_"+DveCislice(aktualni.getDay())+".htm";
		}
		pravidelne[i]["zaltar"] = ((aktualni_tyden +3 ) % 4) + 1;
		pravidelne[i]["barva"] = barva;
		pravidelne[i]["typ"] = "";
		co_se_slavi[i]["kde"] = "P";
		co_se_slavi[i]["id"] = i;
		co_se_slavi[i]["lze_slavit"] = false;
		co_se_slavi[i]["datum"] = PrevedNaDatum(aktualni.getDate(),aktualni.getMonth()+1,aktualni.getFullYear());
		DalsiDen(aktualni);
		i = i+1;
	}
	//document.write(pravidelne.length);
	//document.write("xxxx "+pravidelne[2]["datum"]+pravidelne[2]["nazev"]+"<br/>");
	//document.write("xxxx "+pravidelne[3]["datum"]+pravidelne[3]["nazev"]+"<br/>");
	//document.write(pravidelne["10"]["datum"]);
}

function VlozProPripominku(datum_od,datum_do)
{
  pro_pripominku[pro_pripominku.length] = new Array();
  pro_pripominku[pro_pripominku.length-1]["datum_od"]=datum_od;
  pro_pripominku[pro_pripominku.length-1]["datum_do"]=datum_do;
}

function ZkontrolujProPripominku(datum)
{
  var i=0;
  for (i=0; i<pro_pripominku.length; i++)
  {
    if ((pro_pripominku[i]["datum_od"]<=datum) && (pro_pripominku[i]["datum_do"]>=datum))
    {
      return true;
    }
  }
  return false;
}

function VlozSanctoral(mesic,den,nazev,podnazev,typ,barva,tabulka_lit_dnu,odkaz,ke_msi,k_liturgii_hodin)
{
	var i = sanctoral.length;
	var adresar = "08sanctoral/";	
	sanctoral[i] = new Array();
	sanctoral[i]["mesic"] = mesic;
	sanctoral[i]["den"] = den;
	sanctoral[i]["nazev"] = nazev;
	sanctoral[i]["podnazev"] = podnazev;
	sanctoral[i]["typ"] = typ;
	sanctoral[i]["barva"] = barva;
	sanctoral[i]["tabulka_lit_dnu"] = tabulka_lit_dnu;
	sanctoral[i]["ke_msi"] = ke_msi;
	sanctoral[i]["k_liturgii_hodin"] = k_liturgii_hodin;
	//sanctoral[i]["odkaz"] = odkaz;
	if (i>0)
	{
	  if ((sanctoral[i-1]["mesic"] == mesic) && (sanctoral[i-1]["den"] == den))
	  {
	    sanctoral[i]["odkaz"] = (odkaz ? odkaz : adresar+DveCislice(mesic)+"_"+DveCislice(den)+"_"+i+".htm");
	    //sanctoral[i]["odkaz"] = (odkaz ? odkaz : adresar+DveCislice(mesic)+"_"+DveCislice(den)+".htm");
	  }
	  else
	  {
	    sanctoral[i]["odkaz"] = (odkaz ? odkaz : adresar+DveCislice(mesic)+"_"+DveCislice(den)+".htm");
	  }
	}
	else
	{
	  sanctoral[i]["odkaz"] = (odkaz ? odkaz : adresar+DveCislice(mesic)+"_"+DveCislice(den)+".htm");
	}
}

function PrepocitejSanctoral()
{
	var den,mesic,rok,rozdil;
	for (j=0; j<sanctoral.length; j++)
	{
		den = sanctoral[j]["den"];
		mesic = sanctoral[j]["mesic"];
		rok = zacatek_rok;
		while (RozdilDnu(konec,PrevedNaDatum(den,mesic,rok)) >= 0)
			{				
				rozdil = RozdilDnu(PrevedNaDatum(den,mesic,rok),zacatek);
				if (rozdil >= 0)
				{
					if ((sanctoral[j]["tabulka_lit_dnu"] <= pravidelne[rozdil]["tabulka_lit_dnu"]) && (sanctoral[j]["tabulka_lit_dnu"]!=12))
					{
						co_se_slavi[rozdil]["kde"] = "S";
						co_se_slavi[rozdil]["id"] = j;
					}
					else if ((pravidelne[rozdil]["tabulka_lit_dnu"]>9) && (sanctoral[j]["tabulka_lit_dnu"]==12))
					{
						co_se_slavi[rozdil]["lze_slavit"] = 'S';
					}
					else if ((pravidelne[rozdil]["tabulka_lit_dnu"]>=9) && (sanctoral[j]["tabulka_lit_dnu"]>=10))
					{
						co_se_slavi[rozdil]["lze_slavit"] = 'N';
					}
						
				}
				rok++;
			}
	}
}

function GenerovatSanctoral()
{
	VlozSanctoral(11,12,"sv. Josafata","biskupa a mučedníka","P","C",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,13,"sv. Anežky České","panny","P","B",11,"","formulář vlastní, preface o pannách a řeholnících, lekce feriální, případně ze společných textů; nelze použít 4. anafory"," vše vlastní a ze společných textů, modlitba uprostřed dne feriální");
	VlozSanctoral(11,15,"sv. Alberta Velikého","biskupa a učitele církve","NP","B",12,"","","");
	VlozSanctoral(11,16,"sv. Markéty Skotské","","NP","B",12,"","","");
	VlozSanctoral(11,16,"sv. Gertrudy","panny","NP","B",12,"","","");
	VlozSanctoral(11,17,"sv. Alžběty Uherské","řeholnice","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní hymnus, 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,18,"Posvěcení římských bazilik svatých apoštolů Petra a Pavla","","NP","B",12,"","","");
	VlozSanctoral(11,21,"Zasvěcení Panny Marie v Jeruzalémě","","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(11,22,"sv. Cecilie","panny a mučednice","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,23,"sv. Klementa I.","papeže a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,23,"sv. Kolumbána","opata","NP","B",12,"","","");
	VlozSanctoral(11,24,"sv. Ondřeje Dung-Laca a druhů","kněze, mučedníků","P","C",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,25,"sv. Kateřiny Alexandrijské","panny a mučednice","NP","C",12,"","","");
	VlozSanctoral(11,30,"sv. Ondřeje","apoštola","SV","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(12,1,"sv. Edmunda Kampiána","kněze a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,3,"sv. Františka Xaverského","kněze","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů"," vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,4,"sv. Jana Damašského","kněze a učitele círvke","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,6,"sv. Mikuláše","biskupa","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,7,"sv. Ambrože","biskupa a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,8,"Panny Marie, počaté bez poskrvny prvotního hříchu","","S","B",3,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(12,9,"sv. Jana Didaka Cuauhtlatoatzina","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,12,"Panny Marie Guadalupské","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,11,"sv. Damasa I.","papeže","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,12,"sv. Jany Františky de Chantal","řeholnice","NP","B",12,"");//XXX
	VlozSanctoral(12,12,"Panny Marie Guadalupské","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,13,"sv. Lucie","panny a mučednice","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,14,"sv. Jana od Kříže","kněze a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(12,21,"sv. Petra Kanisia","kněze a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze dne, preface 2. adventní, lekce feriální","vše feriální, v modlitbě se čtením se může po 2. čtení z ferie a jeho responsoriu přidat navíc 2. čtení o svatém se svým responsoriem a závěrečnou modlitbou o svatém, v ranních chvalách a nešporách se po závěrečné modlitbě může navíc přidat antifona k evangelnímu kantiku a modlitba o svatém");
	VlozSanctoral(12,23,"sv. Jana Kentského","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze dne, preface 2. adventní, lekce feriální","vše feriální, v modlitbě se čtením se může po 2. čtení z ferie a jeho responsoriu přidat navíc 2. čtení o svatém se svým responsoriem a závěrečnou modlitbou o svatém, v ranních chvalách a nešporách se po závěrečné modlitbě může navíc přidat antifona k evangelnímu kantiku a modlitba o svatém");
	VlozSanctoral(12,26,"sv. Štěpána","prvomučedníka","SV","C",7,"","Gloria, formulář vlastní, preface o narození Páně, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, nešpory z oktávu Narození Páně, kompletář 1. nebo 2. nedělní");
	VlozSanctoral(12,27,"sv. Jana","apoštola a evangelisty","SV","B",7,"","Gloria, formulář vlastní, preface o narození Páně, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, nešpory z oktávu Narození Páně, kompletář 1. nebo 2. nedělní");
	VlozSanctoral(12,28,"sv. Mláďátek","mučedníků","SV","C",7,"","Gloria, formulář vlastní, preface o narození Páně, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, nešpory z oktávu Narození Páně, kompletář 1. nebo 2. nedělní");
	VlozSanctoral(12,29,"sv. Tomáše Becketa","biskupa a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze dne, preface o Narození Páně, lekce feriální","vše feriální, v modlitbě se čtením se může po 2. čtení z ferie a jeho responsoriu přidat navíc 2. čtení o svatém se svým responsoriem a závěrečnou modlitbou o svatém, v ranních chvalách a nešporách se po závěrečné modlitbě může navíc přidat antifona k evangelnímu kantiku a modlitba o svatém");
	VlozSanctoral(12,31,"sv. Silvestra I.","papeže","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze dne, preface o narození Páně, lekce feriální","vše feriální, v modlitbě se čtením se může po 2. čtení z ferie a jeho responsoriu přidat navíc 2. čtení o svatém se svým responsoriem a závěrečnou modlitbou o svatém, v ranních chvalách a nešporách se po závěrečné modlitbě může navíc přidat antifona k evangelnímu kantiku a modlitba o svatém");
	VlozSanctoral(1,1,"Matky Boží, Panny Marie","","S","B",3,""," Gloria, Krédo, formulář vlastní, preface 1. o Panně Marii (<i>když slavíme její mateřství</i>), v římském kánonu <i>V tomto společenství</i> ze slavnostni Narození Páně, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, v modltibě uprostřed dne v neděli žalmy nedělní z 1. týdne, jinak z doplňovacího cyklu.");
	VlozSanctoral(1,2,"sv. Basila Velikého a Řehoře Naziánského","biskupů a učitelů církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,3,"Nejsvětějšího Jména Ježíš","","NP","B",12,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,6,"Zjevení Páně","","S","B",2,"02vanoce/01_06.htm","Gloria, Krédo, preface o Zjevení Páně, v římském kánonu vlastní <i>V tomto společenství</i>, nelze použít 4. anafory, mohou se ohlašovat pohyblivé svátky příslušného roku","vše vlastní, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní.");
	//VlozSanctoral(1,5,"sv. Jana Nepomuckého Neumanna","biskupa","NP","B",12,"");//XXX
	VlozSanctoral(1,7,"sv. Rajmunda z Penafortu","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,13,"sv. Hilaria","biskupa a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,17,"sv. Antonína","opata","P","B",10,""," formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,18,"Panny Marie, Matky jednoty křesťanů","","P","B",11,"","formulář vlastní, preface o Marii v dějinách spásy, lekce feriální, případně přivlastněné nebo ze společných textů; nelze použít 4. anafory. ","vše vlastní a ze společných textů");
	VlozSanctoral(1,20,"sv. Fabiána","papeže a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,20,"sv. Šebestiána","mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,21,"sv. Anežky","panny a mučednice","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů,lekce feriální, případně ze společných textů; ","vše vlastní a ze společných textů, modlitba uprostřed dne feriální");
	VlozSanctoral(1,22,"sv. Vincence","jáhna a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,24,"sv. Františka Saleského","biskupa a učitele církve","P","B",10,"","orace vlastní; antifony ze společných textů,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,25,"Obrácení sv. Pavla","apoštola","Sv","B",7,"","Gloria, formulář vlastní, preface 1. o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní.");// XXX vlastní?
	VlozSanctoral(1,26,"sv. Timoteje a Tita","biskupů","P","B",10,"","formulář vlastní, 1. čtení vlastní, evangelium přivlastněné nebo ze společných textů nebo feriální","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,27,"sv. Anděly Mericiové","panny","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo feriální,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,28,"sv. Tomáše Akvinského","kněze a učitele církve","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo feriální,lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(1,31,"sv. Jana Boska","kněze","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo feriální,lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(2,2,"Uvedení Páně do chrámu ","","Sv","B",5,"","svěcení svíček a průvod, formulář vlastní, Gloria, v neděli i Krédo, preface vlastní, lekce vlastní - pokud připadne na neděli, má i 2. čtení; nelze použít 4. anafory.","vše vlastní, pokud připadne na neděli, má i 1. nešpory.");
	VlozSanctoral(2,3,"sv. Blažeje","biskupa a mučedníka","NP","C",12,""," kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů, je možné udělovat svatoblažejské požehnání","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(2,3,"sv. Ansgara","biskupa","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	
	VlozSanctoral(2,5,"sv. Agáty","panny a mučednice","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů"," vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(2,6,"sv. Pavla Mikiho a druhů","mučedníků","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(2,8,"sv. Jeronýma Emilianiho","","NP","B",12,"","","");
	VlozSanctoral(2,8,"sv. Josefiny Bakhity","panny","NP","B",12,"","","");
	VlozSanctoral(2,10,"sv. Scholastiky","panny","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(2,11,"Panny Marie Lurdské","","NP","B",12,"","","");
	VlozSanctoral(2,17,"sv. Alexia a druhů","řeholníků","NP","B",12,"","","");
	VlozSanctoral(2,21,"sv. Petra Damianiho","biskupa a učitele církve","NP","B",12,"","","");
	VlozSanctoral(2,22,"Stolce sv. Petra","apoštola","Sv","B",7,"","formulář vlastní, preface 1. o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(2,23,"sv. Polykarpa","biskupa a mučedníka","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(3,4,"sv. Kazimíra","","NP","B",12,"","","");
	VlozSanctoral(3,7,"sv. Perpetuy a Felicity","mučednic","P","C",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(3,8,"sv. Jana z Boha","řeholníka","NP","B",12,"","","");
	VlozSanctoral(3,9,"sv. Františky Římské","řeholnice","NP","B",12,"","","");
	VlozSanctoral(3,10,"sv. Jana Ogilvie","kněze a mučedníka","NP","C",12,"","","");
	VlozSanctoral(3,17,"sv. Patrika","biskupa","NP","B",12,"","","");
	VlozSanctoral(3,18,"sv. Cyrila Jeruzalémského","biskupa a učitele církve","NP","B",12,"","","");
	VlozSanctoral(3,19,"sv. Josefa","Snoubence Panny Marie","S","B",3,"","Gloria, Krédo, preface o sv. Josefu, nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(3,23,"sv. Turibia z Mongroveja","biskupa","NP","B",12,"","","");
	VlozSanctoral(3,25,"Zvěstování Páně","","S","B",3,"","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","Žalmy nedělní z 1. týdne.");
	VlozSanctoral(4,2,"sv. Františka z Pauly","poustevníka","NP","B",12,"","","");
	VlozSanctoral(4,4,"sv. Izidora","biskupa a učitele církve","NP","B",12,"","","");
	VlozSanctoral(4,5,"sv. Vincence Ferrerského","kněze","NP","B",12,"","","");
	VlozSanctoral(4,7,"sv. Jana Křtitele de la Salle","kněze","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(4,11,"sv. Stanislava","biskupa a mučedníka","P","C",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(4,13,"sv. Martina I.","papeže a mučedníka","NP","C",12,"","","");
	VlozSanctoral(4,21,"sv. Anselma","biskupa a učitele církve","NP","B",12,"","","");
	VlozSanctoral(4,23,"sv. Vojtěcha","biskupa a mučedníka","Sv","C",8,"","Gloria, formulář vlastní, lekce vlastní, preface vlastní, nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní.");
	VlozSanctoral(4,24,"sv. Jiřího","mučedníka","NP","C",12,"","","");
	VlozSanctoral(4,24,"sv. Fidela ze Sigmaringy","kněze a mučedníka","NP","C",12,"","","");
	VlozSanctoral(4,25,"sv. Marka","evangelisty","Sv","C",7,"","Gloria, formulář vlastní, preface o 2. o apoštolech, lekce vlastní; není možné použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(4,28,"sv. Petra Chanela","kněze a mučedníka","NP","C",12,"","","");
	VlozSanctoral(4,28,"sv. Ludvíka Marie Grigniona z Montfortu","kněze","NP","B",12,"","","");
	VlozSanctoral(4,29,"sv. Kateřiny Sienské","panny a učitelky církve","Sv","B",7,"","Gloria, formulář vlastní, lekce vlastní","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní.");
	VlozSanctoral(4,30,"sv. Zikmunda","mučedníka","NP","C",12,"","","");
	VlozSanctoral(4,30,"sv. Pia V.","papeže","NP","C",12,"","","");
	VlozSanctoral(5,1,"sv. Josefa, Dělníka","","NP","B",12,"","","");
	VlozSanctoral(5,2,"sv. Atanáše","biskupa a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(5,3,"sv. Filipa a Jakuba","apoštolů","Sv","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(5,6,"sv. Jana Sarkandra","kněze a mučedníka","NP","C",12,"","formulář vlastní, lekce feriální, případně ze společných textů","vše vlastní a ze společných textů");
	VlozSanctoral(5,8,"Panny Marie, Prostřednice všech milostí","","NP","B",12,"","","");
	VlozSanctoral(5,12,"sv. Nerea a Achillea","mučedníků","NP","C",12,"","","");
	VlozSanctoral(5,12,"sv. Pankráce","mučedníka","NP","C",12,"","","");
	VlozSanctoral(5,13,"Panny Marie Fatimské","","NP","B",12,"","","");
	VlozSanctoral(5,14,"sv. Matěje","apoštola","Sv","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(5,16,"sv. Jana Nepomuckého","kněze a mučedníka","Sv","C",8,"","Gloria, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(5,18,"sv. Jana I.","papeže a mučedníka","NP","C",12,"","","");
	VlozSanctoral(5,20,"sv. Klementa Marie Hofbauera","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů"," vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(5,20,"sv. Bernardina Sienského","kněze","NP","B",12,"","","");
	VlozSanctoral(5,21,"sv. Kryštofa Magallanese a druhů","kněze, mučedníků","NP","C",12,"","","");
	VlozSanctoral(5,22,"sv. Rity z Cascie","řeholnice","NP","B",12,"","","");
	
	VlozSanctoral(5,26,"sv. Filipa Neriho","kněze","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(5,27,"sv. Augustina z Canterbury","biskupa","NP","B",12,"","","");
	VlozSanctoral(5,30,"sv. Zdislavy","","NP","B",11,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(5,31,"Navštívení Panny Marie","","SV","B",7,"","gloria, formulář vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením Te Deum, žalmy ranních chval 1. nedělní");
	
	VlozSanctoral(6,1,"sv. Justina","mučedníka","P","Č",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,2,"sv. Marcelina a Petra","mučedníků","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,3,"sv. Karla Lwangy a druhů","mučedníků","P","Č",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,5,"sv. Bonifáce","biskupa a mučednía","P","Č",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,6,"sv. Norberta","biskupa","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,9,"sv. Efréma","jáhna a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	//VlozSanctoral(6,9,"Neposkvrněného Srdce Panny Marie","","P","B",10,"08sanctoral/NSM.htm","formulář vlastní, preface o Panně Marii, 1. čtení a žalm feriální, případně ze společných textů, evangelium vlastní; nelze použít 4. anafory","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");//XXX!!!
	VlozSanctoral(6,11,"sv. Barnabáše","apoštola","P","Č",10,"","formulář vlastní, preface 2. o apoštolech, 1. čtení vlastní, žalm a evangelium feriální, případně přivlastněné nebo ze společných textů; nelze použít 4. anafory","vše vlastní a ze společných textů, modlitba uprostře dne feriální");
	VlozSanctoral(6,13,"sv. Antonína z Padovy","kněze a učitele církve","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
		
	VlozSanctoral(6,15,"sv. Víta","mučedníka","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,19,"sv. Jana Nepomuckého Neumanna","biskupa","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, ekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");//XXX
	VlozSanctoral(6,19,"sv. Romualda","opata","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,21,"sv. Aloise Gonzagy","řeholníka","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,22,"sv. Paulina Nolánského","biskupa","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů")
	VlozSanctoral(6,22,"sv. Jana Fishera, kněze, a Tomáše Mora","mučedníků","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů")	
	VlozSanctoral(6,24,"Narození sv. Jana Křtitele","","S","B",3,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory; v předvečer se použijí texty pro vigilii","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(6,27,"sv. Cyrila Alexandrijského","biskupa a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,28,"sv. Ireneje","biskupa a mučedníka","P","C",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(6,29,"sv. Petra a Pavla","apoštolů","S","C",3,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory; v předvečer se použijí texty z vigilie"," vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(6,30,"Svatých prvomučedníků římských","","NP","Č",12,"","formulář vlastní, preface o Panně Marii, lekce feriální, případně přivlastněné nebo ze společných textů; nelze použít 4. anafory","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,3,"sv. Tomáše"," apoštola","SV","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(7,4,"sv. Prokopa"," opata","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vše vlastní a ze společných textů");
	VlozSanctoral(7,4,"sv. Alžběty Portugalské","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,5,"sv. Cyrila a Metoděje","mnicha a biskupa, patronů Evropy, hlavních patronů Moravy","S","B",5,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(7,6,"sv. Marie Gorettiové","","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(7,9,"sv. Augustina Žao Ronga, kněze a druhů","mučedníků","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů (zpěv před evangeliem přivlastněný)","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,11,"sv. Benedikta"," opata, patrona Evropy","SV","B",7,"","Gloria, formulář vlastní, preface o řeholnících, lekce vlastní; není možné použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(7,13,"sv. Jindřicha","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, preface , lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(7,14,"bl. Hroznaty","mučedníka","NP","Č",12,"","orace vlastní; antifony ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,14,"sv. Kamila de Lellis","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,15,"sv. Bonaventury","","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,16,"Panny Marie Karmelské","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,17,"bl. Česlava a sv. Hyacinta","kněží","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,20,"sv. Apolináře","biskupa a mučedníka","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(7,21,"sv. Vavřince z Brindisi","kněze a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,22,"sv. Marie Magdalény","","SV","B",7,"","Gloria, formulář vlastní, lekce vlastní, preface o svatých","vše vlastní a ze společných textů");
	VlozSanctoral(7,23,"sv. Brigity","řeholnice, patronky Evropy","SV","B",8,"","Gloria, formulář vlastní, lekce vlastní","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(7,24,"sv. Šarbela Machlúfa","kněze","NP","B",12,"","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,25,"sv. Jakuba","apoštola","SV","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(7,26,"sv. Jáchyma a Anny","rodičů Panny Marie","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní hymny, 2. čtení, kolekta, krátká čtení s responsorii k ranním chvalám a nešporám a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,27,"sv. Gorazda a druhů","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,29,"sv. Marty","","P","B",10,"","formulář vlastní, 1. čtení a žalm feriální, případně přivlastněné nebo ze společných textů, evangelium vlastní","vlastní 2. čtení, kolekta, hymnus k nešporám a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(7,30,"sv. Petra Chryzologa","biskupa a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů.");
	VlozSanctoral(7,31,"sv. Ignáce z Loyoly","kněze","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,1,"sv. Alfonsa Marie z Liguori"," biskupa a učitele církve","P","B",10,"","orace vlastní; antifony ze společných textů, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,2,"sv. Eusebia z Vercelli","biskupa","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,2,"Petra Juliána Eymarda","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,4,"sv. Jana Marie Vianneye","kněze","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,5,"Posvěcení římské baziliky Panny Marie","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,6,"Proměnění Páně","","SV","B",5,"","Gloria (v neděli i Krédo), formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, pokud připadne na neděli, má i 1. nešpory");
	VlozSanctoral(8,7,"sv. Sixta II., papeže, a druhů","mučedníků","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");// XXX papeže..
	VlozSanctoral(8,7,"sv. Kajetána","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,8,"sv. Dominika","kněze","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,9,"sv. Terezie Benedikty od Kříže","panny a mučednice, patronky Evropy","SV","C",8,"","Gloria, formulář vlastní, preface o pannách a řeholnících, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(8,10,"sv. Vavřince","jáhna mučedníka","SV","C",7,"","Gloria, formulář vlastní, preface o mučednících, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(8,11,"sv. Kláry","panny","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,12,"sv. Jany Františky de Chantal","řeholnice","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,13,"sv. Ponciána, papeže, a Hippolyta, kněze","mučedníků","NP","Č",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,14,"sv. Maxmiliána Kolbeho","kněze a mučedníka","P","C",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,15,"Nanebevzetí Panny Marie","","S","B",3,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory, v předevečer se použijí texty pro vigilii","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(8,16,"sv. Štěpána Uherského","","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,19,"sv. Jana Eudese","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,20,"sv. Bernarda","opata a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,21,"sv. Pia X.","papeže","P","B",10,"","orace vlastní; antifony ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,22,"Panny Marie Královny","","P","B",10,"","formulář vlastní, preface , lekce feriální, případně ze společných textů; nelze použít 4. anafory","vlastní hymny, 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů.");//xxx preface
	VlozSanctoral(8,23,"sv. Růženy z Limy","panny","NP","B",12,"","","");
	VlozSanctoral(8,24,"sv. Bartoloměje","apoštola","SV","Č",7,"","gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením Te Deum, žalmy ranních chval 1. nedělní.");
	VlozSanctoral(8,25,"sv. Ludvíka","","NP","B",12,"","","");
	VlozSanctoral(8,25,"sv. Benedikta, Jana, Marouše, Izáka a Kristina","mučedníků","NP","Č",12,"","","");
	VlozSanctoral(8,25,"sv. Josefa Kalasanského","kněze","NP","B",12,"","","");
	VlozSanctoral(8,27,"sv. Moniky","","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,28,"sv. Augustina","biskupa a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(8,29,"Umučení sv. Jana Křtitele","","P","C",10,"","formulář vlastní, preface vlastní, 1. čtení a žalm feriální, případně přivlastněné nebo ze společných textů, evangelium vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, modlitba uprostřed dne feriální");

	VlozSanctoral(9,3,"sv. Řehoře Velikého","papeže a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,7,"sv. Melichara Grodeckého","kněze a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,8,"Narození Panny Marie","","SV","B",7,"","Gloria, formulář vlastní, preface 1. o Panně Marii (když slavíme její narození), lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(9,9,"sv. Petra Klavera","kněze","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,10,"bl. Karla Spinoly","kněze a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,12,"Jména Panny Marie","","NP","B",12,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,13,"sv. Jana Zlatoústého","biskupa a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,14,"Povýšení sv. Kříže","","SV","C",5,"","Gloria (v neděli i Krédo), formulář vlastní, preface vlastní, lekce vlastní (v neděli navíc i 2. čtení); nelze použít 4. anafory"," vše vlastní, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, připadne-li na neděli, má i 1. nešpory");

	VlozSanctoral(9,15," Panny Marie Bolestné ","","P","B",10,"","formulář vlastní, 1. čtení a žalm feriální nebo ze společných textů, evangelium vlastní; po žalmu lze vložit sekvenci, preface o Panně Marii, nelze použít 4. anafory"," vše vlastní nebo ze společných textů");
	VlozSanctoral(9,16,"sv. Ludmily","mučednice","P","C",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vše vlastní a ze společných textů");
	VlozSanctoral(9,17,"sv. Kornélia a Cypriána","papeže a biskupa, mučedníků","NP","C",12,"","orace vlastní; antifony ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,17,"sv. Roberta Bellarmina","biskupa a učitele církve","NP","B",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,19,"sv. Januária","biskupa a mučedníka","NP","C",12,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,20,"sv. Ondřeje Kim Taegona, Pavla Chong Hasanga a druhů","mučedníků","P","C",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,21,"sv. Matouše","apoštola a evangelisty","Sv","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(9,23,"sv. Pia z Pietrelciny","kněze","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,26,"sv. Kosmy a Damiána","mučedníků","NP","C",12,"","orace vlastní; antifony ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,27,"sv. Vincence z Pauly","kněze","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(9,28,"sv. Václava","mučedníka, hlavního patrona českého národa","S","Č",4,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(9,29,"sv. Michaela, Gabriela a Rafaela","archandělů","SV","B",7,"","Gloria, formulář vlastní, preface o andělech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(9,30,"sv. Jeronýma","kněze a učitele církve","P","B",10,"","kolekta vlastní; ostatní části formuláře ze společných textů nebo ze dne, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");

	VlozSanctoral(10,1,"sv. Terezie od Dítěte Ježíše","panny","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(10,2,"svatých andělů strážných","","P","B",10,"","formulář vlastní, preface o andělech, 1. čtení a žalm feriální nebo přivlastněné nebo ze společných textů, evangelium vlastní; nelze použít 4. anafory","vše vlastní kromě žalmů s antifonami k modlitbě se čtením a modlitbě uprostřed dne, které jsou feriální");
	VlozSanctoral(10,4,"sv. Františka z Assisi","","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní hymny, 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(10,5,"sv. Faustiny Kowalské","panny","NP","B",12,"","","");
	VlozSanctoral(10,6,"sv. Bruna","kněze","NP","B",12,"","","");
	VlozSanctoral(10,7,"Panny Marie Růžencové","","P","B",10,"","formulář vlastní, preface o Panně Marii, lekce feriální, případně ze společných textů; nelze použít 4. anafory","vše vlastní a ze společných textů, modlitba uprostřed dne feriální");
	VlozSanctoral(10,9,"sv. Dionýsia a druhů","biskupa, mučedníků","NP","C",12,"","","");
	VlozSanctoral(10,9,"sv.  Jana Leonardiho","kněze","NP","B",12,"","","");
	VlozSanctoral(10,11,"sv. Jana XIII.","papeže","NP","B",12,"","","");
	VlozSanctoral(10,12,"sv. Radima","biskupa","NP","B",12,"","","");
	VlozSanctoral(10,14,"sv. Kalista I.","papeže a mučedníka","NP","C",12,"","","");
	VlozSanctoral(10,15,"sv. Terezie od Ježíše","panny a učitelky církve","P","B",10,"","formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní hymny, 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(10,16,"sv. Markéty Marie Alacoque","panny","NP","B",12,"","","");
	VlozSanctoral(10,16,"sv. Hedviky","řeholnice, hlavní patronky Slezska","NP","B",12,"","","");
	VlozSanctoral(10,17,"sv.  Ignáce Antiochijského","biskupa a mučedníka","P","C",10,""," formulář vlastní, lekce feriální, případně přivlastněné nebo ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(10,18,"sv. Lukáše","evangelisty","Sv","C",7,"","Gloria, formulář vlastní, preface 2. o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(10,19,"sv. Jana de Brébeuf a Izáka Joguese a druhů","kněží a mučedníků","NP","C",12,"","","");
	VlozSanctoral(10,19,"sv. Pavla od Kříže","kněze","NP","B",12,"","","");
	VlozSanctoral(10,21,"bl. Karla Rakouského","","NP","B",12,"","","");
	VlozSanctoral(10,22,"sv. Jana Pavla II.","papeže","NP","B",12,"","","");
	VlozSanctoral(10,23,"sv. Jana Kapistránského","kněze","NP","C",12,"","","");
	VlozSanctoral(10,24,"sv. Antonína Marie Klareta","biskupa","NP","B",12,"","","");
	//VlozSanctoral(10,25,"Výročí posvěcení chrámu","jejichž den dedikace není známý","S","B",3,"","","");
	VlozSanctoral(10,28,"sv. Šimona a Judy","apoštolů","Sv","C",7,"","Gloria, formulář vlastní, preface o apoštolech, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	//VlozSanctoral(10,29,"bl. Marie Restituty Kafkové","panny a mučednice","NP","C",12,"","","");
	VlozSanctoral(10,31,"sv. Wolfganga","biskupa","NP","B",12,"","","");
	VlozSanctoral(11,1,"Všech svatých","","S","B",3,"","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní");
	VlozSanctoral(11,2,"Vzpomínka na všechny věrné zemřelé","","","F",3,"","3 formuláře vlastní, preface za zemřelé, lekce ze společných textů za zemřelé (3 čtení, lekcionář VI/2); nelze použít 4. anafory","vlastní 2. čtení a kolekta, hymny k ranním chvalám a nešporám, vše ostatní z textů za zemřelé. V neděli se používají texty z neděle – slaví-li se ale ranní chvály či nešpory za účasti lidu, je možné použít textů za zemřelé.");
	VlozSanctoral(11,3,"sv. Martina de Porres","řeholníka","NP","B",12,"","","");
	VlozSanctoral(11,4,"sv. Karla Boromejského","biskupa","P","B",10,"","orace vlastní; antifony ze společných textů, lekce feriální, případně ze společných textů","vlastní 2. čtení a kolekta, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,9,"Posvěcení lateránské baziliky","","Sv","B",5,"","Gloria (v neděli i Krédo), formulář o posvěcení kostela, preface vlastní, lekce vlastní (v neděli i 2. čtení); nelze použít 4. anafory","vlastní 2. čtení a kolekta, vše vlastní a ze společných textů, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, pokud připadne na neděli, má i 1. nešpory");
	VlozSanctoral(11,10,"sv. Lva Velikého","papeže a učitele církve","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vlastní 2. čtení, kolekta a antifony k Benedictus a Magnificat, vše ostatní feriální nebo ze společných textů");
	VlozSanctoral(11,11,"sv. Martina","biskupa","P","B",10,"","formulář vlastní, lekce feriální, případně ze společných textů","vše vlastní a ze společných textů, modlitba uprostřed dne feriální");

		
	PrepocitejSanctoral();
}

function GenerovatVyjimky()
{
VlozVyjimky(PrevedNaDatum(22,11,2020),"Ježíše Krista Krále","","S","B",3,"06mezidobi/Krista_Krale.htm","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",1,1);
//VlozVyjimky(PrevedNaDatum(9,12,2020),"Panny Marie, počaté bez poskrvny prvotního hříchu","","S","B",3,"08sanctoral/12_08.htm","Gloria, Krédo, formulář vlastní, preface vlastní, lekce vlastní; nelze použít 4. anafory","vše vlastní a ze společných textů, má také 1. nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní",1,1);
VlozVyjimky(PrevedNaDatum(13,12,2020),"3. neděle adventní","","","R",2,"01advent/03_00.htm","Gloria se neříká, Krédo, preface vlastní. Nelze použít 4. anafory","Žalmy nedělní z 1. týdne.",1,1);
VlozVyjimky(PrevedNaDatum(25,12,2020),"Narození Páně","","S","B",2,"02vanoce/12_25.htm","Gloria, Krédo (při slovech <em>Skrze Ducha Svatého... stal se člověkem</em> se klečí), preface o Narození Páně, užívá-li se římského kánonu, říká se vlastní <em>V tomto společenství</em>, nelze použít 4. anafory","Modlitbu před spaním po 1. nešporách říkají pouze ti, kdo se nezúčastní slavení modlitby s čtením a mše v noci. Hymnus vánoční, vše ostatní vlastní, v ranních chválách žalmy nedělní z 1. týdne. Modlitba před spaním jako po 1. nebo 2. nedělních nešporách.",1,0);
VlozVyjimky(PrevedNaDatum(27,12,2020),"Rodiny Ježíše, Marie a Josefa","","SV","B",5,"02vanoce/SvRodina.htm","Gloria, Krédo, preface o Narození Páně, užívá-li se římského kánonu, říká se vlastní <em>V tomto společenství</em>","Připadne-li na neděli, má první nešpory. V prvních a druhých nešporách, modlitbě se čtením žalmy a kantikum ze společných textů o Panně Marii. V modlitbě se čtením chvalozpěv <i>Te Deum</i>. V ranních chválách žalmy nedělní z 1. týdne. V modlitbě uprostřed dne v neděli žalmy nedělní z 1. týdne, jinak páteční z 1. týdne. Modlitba před spaním jako po 1. nebo 2. nedělních nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(31,12,2020),"31. prosince","","","B",9,"02vanoce/12_31.htm","Gloria, preface o Narození Páně, v římském kánonu <i>V tomto společenství</i> ze slavnostni Narození Páně","V modlitbě se čtením vánoční hymnus a vše ostatní vlastní, na závěr chvalozpěv <i>Te Deum</i>. V ranních chválách žalmy nedělní z 1. týdne. V modlitbě uprostřed žalmy z příslušného dne v týdnu. Nešpory a modlitba před spaním z následující slavnosti.",1,1);//XXX
VlozVyjimky(PrevedNaDatum(10,1,2021),"Křtu Páně","","Sv","B",5,"02vanoce/KrtuPane.htm","Gloria, 1. a 2. čtení se žalmem mohou být z cyklu A, Krédo, preface vlastní. Nelze použít 4. anafory","vše vlastní, má první nešpory, v modlitbě se čtením <i>Te Deum</i>, žalmy ranních chval 1. nedělní, žalmy modlitby uprostřed dne 3. nedělní, v neděli 7. ledna však 2. nedělní, je-li slavnost Zjevení Páně přeložena na neděli 7. nebo 8. ledna, žalmy pondělní z 1. týdne, žalmy 2. nešpor ze Zjevení Páně",0,1);
VlozVyjimky(PrevedNaDatum(17,2,2021),"Popeleční středa","","","F",2,"03pust/00_03.htm","místo úkonu kajícnosti žehnání popela a označování věřících, po homilii žehnání popela, preface 3. nebo 4. postní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","Žalmy středeční ze 4. týdne, v ranních chválách se mohou vzít z pátku 3. týdne. Krátká čtení se zpěvy, prosby a závěrečná modlitba vlastní.",1,1);
VlozVyjimky(PrevedNaDatum(21,2,2021),"1. neděle postní","","","F",2,"03pust/01_00.htm","na začátku mše lze žehnat popel a označovat věřící, Krédo, preface vlastní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(28,2,2021),"2. neděle postní","","","F",2,"03pust/02_00.htm","Krédo, preface vlastní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(7,3,2021),"3. neděle postní","","","F",2,"03pust/03_00.htm","Krédo, čte-li se evangelium o samařské ženě: preface vlastní, jinak preface 1. nebo 2. postní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(14,3,2021),"4. neděle postní","","","R",2,"03pust/04_00.htm","Krédo, čte-li se evangelium o slepém od narození: preface vlastní, jinak preface 1. nebo 2. postní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(21,3,2021),"5. neděle postní","","","F",2,"03pust/05_00.htm","Krédo, čte-li se evangelium o Lazarovi: preface vlastní, jinak preface 1. nebo 2. postní, na konci mše modlitba nad lidem. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(28,3,2021),"Květná neděle","","","C",2,"03pust/06_00.htm","Žehnání ratolestí a průvod, Krédo, preface vlastní. Nelze použít 4. anafory","krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách a nešporách i antifony vlastní, v modlitbě se čtením se vynechává <i>Te Deum</i>",1,1);
VlozVyjimky(PrevedNaDatum(1,4,2021),"Zelený čtvrtek","","","B",1,"04triduum/01ctvrtek.htm","Gloria, obřad mytí nohou, preface 1. o eucharistii, v římském kánonu vlastní <i>V tomto společenství..., Přijmi tedy..., Neboť on právě dnes...</i>, nelze použít 4. anafory, přenesení Nejsvětější svátosti","Krátké čtení se zpěvem, prosby a závěrečná modlitba a v ranních chválách i antifony vlastní ze čtvrtku Svatého týdne. Nešpory ze Zeleného čtvrtku se modlí pouze ti, kdo se nezúčastní večerní mše na památku Večeře Páně. V kompletáři vlastní hymnus a zpěv po krátkém čtení, ostatní jako v neděli po 2. nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(2,4,2021),"Velký pátek","","","C",1,"04triduum/02patek.htm","Podle prastaré tradice dnes neslaví církev eucharistickou oběť. Památka umučení Páně obsahuje bohoslužbu slova, přímluvy, obřad uctívání kříže a svaté přijímání.","Vše vlastní. Nešpory se modlí pouze ti, kdo se nezúčastní slavení památky umučení Páně, doporučuje se použít přímluv z misálu. V kompletáři vlastní zpěv po krátkém čtení, ostatní jako v neděli po 2. nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(3,4,2021),"Bílá sobota","","","F",1,"04triduum/03sobota.htm","Podle prastaré tradice dnes neslaví církev eucharistickou oběť.","Vše vlastní. Kompletář se modlí pouze ti, kdo se nezúčastní slavení velikonoční vigilie, má vlastní zpěv po krátkém čtení, ostatní jako v neděli po 2. nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(4,4,2021),"Zmrtvýchvstání Páně","","S","B",1,"04triduum/05vzkriseni.htm","jako úkon kajícnosti je vhodné kropení lidu vodou posvěcenou při vigilii, Gloria, sekvence, Krédo, preface 1. velikonoční (<i>zvláště nyní o Velikonocích</i>), v římském kánonu vlastní <i>V tomto společenství...</i> a <i>Přijmi tedy</i>, nelze použít 4. anafory, na závěr se mohou žehnat pokrmy, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>, večerní mše má vlastní evangelium","Vše vlastní. Modlitbu se čtením se modlí pouze ti, kdo se nezúčastní velikonoční vigilie, začíná se bezprostředně čteními, čtou se přitom alespoň čtyři čtení z této vigilie (včetně evangelia) s příslušnými mezizpěvy a modlitbami, na závěr <i>Te Deum</i>. doporučuje se použít přímluv z misálu. Následuje-li propuštění lidu, říká se <i>Jděte ve jménu Páně, aleluja, aleluja.</i> V kompletáři vlastní zpěv po krátkém čtení, ostatní jako v neděli po 1. nebo 2. nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(11,4,2021),"2. neděle velikonoční (Božího milosrdenství)","","","B",2,"05velikonoce/02_00.htm","Gloria, lze vložit sekvenci, Krédo, preface 1. velikonoční (<i>zvláště nyní o Velikonocích</i>), v římském kánonu vlastní <i>V tomto společenství...</i> a <i>Přijmi tedy</i>, nelze použít 4. anafory, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>","V modlitbě se čtením hymnus velikonoční, ostatní vlastní, na závěr <i>Te Deum</i>. V ranních chválách a nešporách antifony, žalmy a kantikum jako v neděli Zmrtvýchvstání Páně, krátké čtení se zpěvem, prosby a závěrečná modlitba vlastní. Následuje-li propuštění lidu, říká se <i>Jděte ve jménu Páně, aleluja, aleluja.</i> Modlitba během dne z neděle Zmrtvýchvstání Páně. V kompletáři vlastní zpěv po krátkém čtení, ostatní jako v neděli po 1. nebo 2. nešporách.",1,1);
VlozVyjimky(PrevedNaDatum(13,5,2021),"Nanebevstoupení Páně","","S","B",2,"05velikonoce/06_04.htm","Gloria, Krédo, preface o Nanebevstoupení Páně, v římském kánonu vlastní <i>V tomto společenství...</i>, nelze použít 4. anafory","Vše vlastní. V modlitbě se čtením <i>Te Deum</i>. V ranních chválách žalmy 1. nedělní." ,1,0);
VlozVyjimky(PrevedNaDatum(23,5,2021),"Seslání Ducha Svatého","","S","C",2,"05velikonoce/07_07a.htm","Gloria, sekvence, Krédo, preface vlastní, v římském kánonu vlastní <i>V tomto společenství...</i>, nelze použít 4. anafory, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>","Vše vlastní. V modlitbě se čtením <i>Te Deum</i>. Následuje-li propuštění lidu, říká se <i>Jděte ve jménu Páně, aleluja, aleluja.</i>",1,1);
VlozVyjimky(PrevedNaDatum(24,5,2021),"Panny Marie, Matky církve","","P","B",10,"08sanctoral/MatkaCirkve.htm","formulář votivní o Panně Marii, Matce církve, preface Maria v dějinách spásy, lekce vlastní; nelze použít 4. anafory","vlastní hymnus modlitby se čtením, ranních chval a nešpor, 2. čtení, antifony k Benedictus a Magnificat a kolekta, vše ostatní feriální nebo ze společných textů",1,1);//XXX!!!"Seslání Ducha Svatého","","S","C",2,"05velikonoce/07_07a.htm","Gloria, sekvence, Krédo, preface vlastní, v římském kánonu vlastní <i>V tomto společenství...</i>, nelze použít 4. anafory, nakonec <i>Jděte ve jménu Páně, aleluja, aleluja.</i>","Vše vlastní. V modlitbě se čtením <i>Te Deum</i>. Následuje-li propuštění lidu, říká se <i>Jděte ve jménu Páně, aleluja, aleluja.</i>",1,1);
VlozVyjimky(PrevedNaDatum(27,5,2021),"Ježíše Krista, nejvyššího a věčného kněze","","Sv","B",8,"08sanctoral/JK_knez.htm","Gloria, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",0,1);//XXX
VlozVyjimky(PrevedNaDatum(30,5,2021),"Nejsvětější Trojice","","S","B",3,"06mezidobi/NST.htm","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",1,1);
VlozVyjimky(PrevedNaDatum(3,6,2021),"Těla a Krve Páně","","S","B",3,"06mezidobi/Tela_Krve.htm","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",1,1);
VlozVyjimky(PrevedNaDatum(11,6,2021),"Nejsvětějšího Srdce Ježíšova","","S","B",3,"06mezidobi/BSP.htm","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",1,1);
VlozVyjimky(PrevedNaDatum(12,6,2021),"Neposkvrněného Srdce Panny Marie","","P","B",10,"08sanctoral/NSM.htm","formulář vlastní, preface o Panně Marii, 1. čtení a žalm feriální, případně ze společných textů, evangelium vlastní; nelze použít 4. anafory","vlastní 2. čtení, kolekta a antifona k Benedictus, vše ostatní feriální nebo ze společných textů",0,1);
VlozVyjimky(PrevedNaDatum(21,11,2021),"Ježíše Krista Krále","","S","B",3,"06mezidobi/Krista_Krale.htm","Gloria, Krédo, preface vlastní. Nelze použít 4. anafory","V ranních chválách žalmy nedělní z 1. týdne.",1,1);

	PrepocitejVyjimky();
}

function VlozVyjimky(datum,nazev,podnazev,typ,barva,tabulka_lit_dnu,odkaz,ke_msi,k_liturgii_hodin,prednost,vynech_pripadne)
{
	var i = vyjimky.length;
	var adresar = "08vyjimky/";	
	vyjimky[i] = new Array();
	vyjimky[i]["datum"] = datum;
	vyjimky[i]["nazev"] = nazev;
	vyjimky[i]["podnazev"] = podnazev;
	vyjimky[i]["typ"] = typ;
	vyjimky[i]["barva"] = barva;
	vyjimky[i]["tabulka_lit_dnu"] = tabulka_lit_dnu;
	vyjimky[i]["odkaz"] = (odkaz ? odkaz : adresar+DveCislice(mesic)+"_"+DveCislice(den)+".htm");
	vyjimky[i]["ke_msi"] = ke_msi;
	vyjimky[i]["k_liturgii_hodin"] = k_liturgii_hodin;
	vyjimky[i]["prednost"] = prednost;
	vyjimky[i]["vynech_pripadne"] = vynech_pripadne;
}

function PrepocitejVyjimky()
{
	var den,mesic,rok,rozdil;
	for (j=0; j<vyjimky.length; j++)
	{
		datum = vyjimky[j]["datum"];
		rozdil = RozdilDnu(datum,zacatek);
		if ((rozdil >= 0) && (rozdil <= celkem_dnu))
		{
			if (vyjimky[j]["prednost"]==1)
			{
              co_se_slavi[rozdil]["kde"] = "V";
			  co_se_slavi[rozdil]["id"] = j;
			}
			else if (co_se_slavi[rozdil]["kde"] == "P")
			{
			  if ((vyjimky[j]["tabulka_lit_dnu"] < pravidelne[rozdil]["tabulka_lit_dnu"]) && (vyjimky[j]["tabulka_lit_dnu"]!=12))
			  {
			    
			    co_se_slavi[rozdil]["kde"] = "V";
			    co_se_slavi[rozdil]["id"] = j;
			  }
			}
			else if (co_se_slavi[rozdil]["kde"] == "S")
			{
			  if ((vyjimky[j]["tabulka_lit_dnu"] < sanctoral[co_se_slavi[rozdil]["id"]]["tabulka_lit_dnu"])  && (vyjimky[j]["tabulka_lit_dnu"]!=12)) 	
			  {
			    co_se_slavi[rozdil]["kde"] = "V";
			    co_se_slavi[rozdil]["id"] = j;
			  }
			}	 
		}
	}
}



function AktualniVypisovat(j)
{
		vypisovat = new Array();
		if (co_se_slavi[j]["kde"]=="P")
		{
			vypisovat = pravidelne[co_se_slavi[j]["id"]];
		}
		else if (co_se_slavi[j]["kde"]=="S")
		{
			
			vypisovat = sanctoral[co_se_slavi[j]["id"]];			
		}
		else if (co_se_slavi[j]["kde"]=="V")
		{
			
			vypisovat = vyjimky[co_se_slavi[j]["id"]];			
		}
		vypisovat["datum"] = co_se_slavi[j]["datum"];
}

function Datum(datum)
{
	return datum.getDate()+'. '+Mesic[datum.getMonth()]+' '+datum.getFullYear();
}

function PrvniVelke(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function VypisMesice()
{
	var akt = zacatek;
	var mesic = akt.getMonth();
	var rok = akt.getFullYear();
	var predchozi = "";
	while (RozdilDnu(akt,zacatek)  <= celkem_dnu)
	{
		if (predchozi)
		{
			document.write(predchozi+" • ");
		}
		predchozi = "<a href=#"+rok+"_"+(DveCislice(mesic))+">"+Mesic_1p[mesic]+"</a>";
		mesic++;
		if (mesic == 12)
		{
			mesic = 0;
			rok++;
		}
		akt = PrevedNaDatum(1,mesic+1,rok);
	}
	document.write(predchozi);
}

function Vypis()
{
	var barva, odkaz, trida;
	//vypisovat = pravidelne;
	//document.write("<table class='kalendar' rules='all'>");
	VypisMesice();
	var AktualniRozdil = RozdilDnu(dnes,zacatek);
	for (j=0; j<=celkem_dnu; j++)
	{
		AktualniVypisovat(j);
		if ((j!=0) && (vypisovat['datum'].getDate()==1))
		{
			document.write("</table>");
		}
		if ((j==0) || (vypisovat['datum'].getDate()==1))
		{
			document.write("<a name="+vypisovat['datum'].getFullYear()+"_"+(DveCislice(vypisovat['datum'].getMonth()))+"></a><div class='mesic'>"+PrvniVelke(Mesic_1p[vypisovat['datum'].getMonth()])+" "+vypisovat['datum'].getFullYear()+"</div>");
			document.write("<table class='kalendar' rules='all'>");
		}
		//styl = "color: "+Barva(vypisovat['barva'])+";";
		trida = '';
		if (vypisovat['datum'].getDay()==0)
		{
			trida += 'nedele ';
		}
		if (vypisovat['tabulka_lit_dnu']<=6)
		{
			trida += 'vyznamne ';
		}
		if (vypisovat['tabulka_lit_dnu']<=4)
		{
			trida += 'nejvyznamnejsi ';
		}
		if (j==AktualniRozdil)
		{
			trida += 'dnes ';
		}
		//style='border:1px black solid;
		odkaz = "<a href='"+vypisovat['odkaz']+"' class='kalendar_odkaz "+Barva(vypisovat['barva'])+"'>";
		//document.write("<tr class='"+Barva(vypisovat['barva'])+" "+trida+"'><td width='10%' align='right'>"+odkaz+vypisovat['datum'].getDate()+".</a></td><td width='70%'>"+odkaz+PrvniVelke(vypisovat['nazev'])+"</td></tr>");
		document.write("<tr class='"+Barva(vypisovat['barva'])+" "+trida+"'><td width='10%' align='right'>"+odkaz+vypisovat['datum'].getDate()+".</a></td><td width='70%'>"+odkaz+PrvniVelke(vypisovat['nazev'])+"</td></tr>");
	}
	document.write("</table>");
}

function TlacitkoKalendar()
{
	document.write('<p><a href="kalendar.htm#'+dnes.getFullYear()+'_'+DveCislice(dnes.getMonth())+'"><button class="menu">Kalendář</button></a></p>');
}

function Tlacitka()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	var nedele = rozdil - dnes.getDay() + 7;
	if (!((nedele < 0) || (rozdil > celkem_dnu)))
	{
	document.write('<table><tr><td align="left" width="142px" rowspan="2">');
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		document.write('<a href="'+vypisovat['odkaz']+'"><button class="menu_pul">Dnes</button></a></td>');
	}
	document.write('</td><td align="right" width="142px">');
	if ((rozdil>=-1) && (rozdil<celkem_dnu))
	{
		AktualniVypisovat(rozdil+1);
		document.write('<a href="'+vypisovat['odkaz']+'"><button class="menu_ctvrt">> Zítra ></button></a><br/>');
	}
	document.write('</td></tr><tr><td height="32px">');
	if ((nedele>=0) && (nedele<=celkem_dnu) && (dnes.getDay()!=6))
	{
		AktualniVypisovat(nedele);
		document.write('<a href="'+vypisovat['odkaz']+'"><button class="menu_ctvrt">>> Neděle >></button></a>');
	}
	document.write('</td></tr></table>');
	}
}

function TlacitkaJakoOdkazy()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	var nedele = rozdil - dnes.getDay() + 7;
	if (!((nedele < 0) || (rozdil > celkem_dnu)))
	{
	//document.write('<table><tr><td align="left" width="142px" rowspan="2">');
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		//document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" target="_blank">Dnes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" target="_blank">Dnes</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	}
	//document.write('</td><td align="right" width="142px">');
	if ((rozdil>=-1) && (rozdil<celkem_dnu))
	{
		AktualniVypisovat(rozdil+1);
		//document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" target="_blank">Zítra</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
		document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" target="_blank">Zítra</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	}
	//document.write('</td></tr><tr><td height="32px">');
	if ((nedele>=0) && (nedele<=celkem_dnu) && (dnes.getDay()!=6))
	{
		AktualniVypisovat(nedele);
		document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" target="_blank">Neděle</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	}
	if ((rozdil>=-1) && (rozdil<celkem_dnu))
	{
		AktualniVypisovat(nedele);
		document.write('<a href="http://m.liturgie.cz/misal/kalendar.htm#'+dnes.getFullYear()+'_'+DveCislice(dnes.getMonth())+'" target="_blank">Kalendář</a>');
	}
	//document.write('</td></tr></table>');
	}
}

function TlacitkoDnes()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		document.write('<a href="../'+vypisovat['odkaz']+'"><button class="menu">Dnes</button></a>');
	}
}


function OdkazDnes()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		return 'http://m.liturgie.cz/misal/'+vypisovat['odkaz'];
	}
}

function OdkazZitra()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	if ((rozdil>=-1) && (rozdil<celkem_dnu))
	{
		AktualniVypisovat(rozdil+1);
		return 'http://m.liturgie.cz/misal/'+vypisovat['odkaz'];
	}
}

function OdkazNedele()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	var nedele = rozdil - dnes.getDay() + 7;
	if ((nedele>=0) && (nedele<=celkem_dnu))
	{
		AktualniVypisovat(nedele);
		return 'http://m.liturgie.cz/misal/'+vypisovat['odkaz'];
	}
}

function OdkazKalendar()
{
  return 'http://m.liturgie.cz/misal/kalendar.htm#'+dnes.getFullYear()+'_'+DveCislice(dnes.getMonth());
}


function JmenoURL()
{
//this gets the full url
var url = document.location.href;
var konec = url.substring(url.lastIndexOf('/')+1);
if (konec.lastIndexOf('#')>0)
{
  konec = konec.substring(0,konec.lastIndexOf('#'));
}
url = url.substring(0,url.lastIndexOf('/'));
url = url.substring(url.lastIndexOf('/')+1);
url = url+"/"+konec;
//this removes the anchor at the end, if there is one
/*url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
//this removes the query after the file name, if there is one
url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
//this removes everything before the last slash in the path
url = url.substring(url.lastIndexOf("/") + 1, url.length);*/
//return
return url;
}

function VypisDatum(datum)
{
  return datum.getDate()+". "+Mesic[datum.getMonth()]+" "+datum.getFullYear();
}

function VypisDen(j)
{
  AktualniVypisovat(j);
  trida = '';
  if (vypisovat['datum'].getDay()==0)
  {
    trida += 'nedele ';
  }
  if (vypisovat['tabulka_lit_dnu']<=6)
  {
    trida += 'vyznamne ';
  }
  if (vypisovat['tabulka_lit_dnu']<=4)
  {
    trida += 'nejvyznamnejsi ';
  }
  odkaz = "<a href='../"+vypisovat['odkaz']+"' class='kalendar_odkaz "+Barva(vypisovat['barva'])+"'>";
  return "<span class='"+Barva(vypisovat['barva'])+" "+trida+"'><button onclick='location.href=\"../"+vypisovat['odkaz']+"\"'; class=\"m\">"+odkaz+Typ(vypisovat['typ'])+" "+PrvniVelke(vypisovat['nazev'])+"</button></a></span>";

}


function VypisSanctoralDatum(Datum,nazev_vynechat)
{
  var rok = Datum.getFullYear();
  var j=0;
  var hlaska = "";
  for (j=0; j<sanctoral.length; j++)
  {
    if ((sanctoral[j]["den"] == Datum.getDate()) && (sanctoral[j]["mesic"]-1 == Datum.getMonth()) && (sanctoral[j]["nazev"]!=nazev_vynechat))
	    //if (PrevedNaDatum(sanctoral[j]["den"],sanctoral[j]["mesic"],rok)==Datum)
    {
      //hlaska += "<button onclick='location.href=\"../"+sanctoral[j]['odkaz']+"\"'; class=\"m\">"+PrvniVelke(sanctoral[j]['nazev'])+"</button>";
      if (hlaska || nazev_vynechat)
      {
	hlaska += ", anebo ";
      }
      hlaska += '<span class = "'+Barva(sanctoral[j]['barva'])+'">';
      if (sanctoral[j]['typ'] =="P")
      {
	hlaska += '<a href="../'+sanctoral[j]['odkaz']+'" class="kalendar_odkaz"><button class="'+Barva(sanctoral[j]['barva'])+'">';
      }
      hlaska += sanctoral[j]['nazev'];
      if (sanctoral[j]['podnazev'])
      {
	hlaska += ", "+sanctoral[j]['podnazev'];
      }
      if (sanctoral[j]['typ'] =="P")
      {
	hlaska += "</button></a>";
      }
      hlaska += "</span>";
      // "../"+sanctoral[j]['odkaz'])
      //+odkaz+Typ(vypisovat['typ'])+" "
    }
  }
  return hlaska;
}

function DirektarSanktoral(direktar_sanct, online=false, online_nedele=false)
{
    var pust = ((sanctoral[direktar_sanct]["mesic"]==2) && (sanctoral[direktar_sanct]["den"]>=5) || (sanctoral[direktar_sanct]["mesic"]==3) || ((sanctoral[direktar_sanct]["mesic"]==4) && (sanctoral[direktar_sanct]["den"]<=24))) ? 1 : 0;
    var velikonoce = ((sanctoral[direktar_sanct]["mesic"]==3) && (sanctoral[direktar_sanct]["den"]>=22) || (sanctoral[direktar_sanct]["mesic"]==4) || (sanctoral[direktar_sanct]["mesic"]==5) || ((sanctoral[direktar_sanct]["mesic"]==6) && (sanctoral[direktar_sanct]["den"]<=13))) ? 1 : 0;
    var advent = ((sanctoral[direktar_sanct]["mesic"]==11) && (sanctoral[direktar_sanct]["den"]>=27) || ((sanctoral[direktar_sanct]["mesic"]==12) && (sanctoral[direktar_sanct]["den"]<=24))) ? 1 : 0;
    var vzdy_pust = ((sanctoral[direktar_sanct]["mesic"]==3) && (sanctoral[direktar_sanct]["den"]>=10) && (sanctoral[direktar_sanct]["den"]<=21)) ? 1 : 0;
    var vzdy_velikonoce = ((sanctoral[direktar_sanct]["mesic"]==4) && (sanctoral[direktar_sanct]["den"]>=25) || ((sanctoral[direktar_sanct]["mesic"]==5) && (sanctoral[direktar_sanct]["den"]<=10))) ? 1 : 0;
    var vzdy_pust_nebo_velikonoce = ((sanctoral[direktar_sanct]["mesic"]==3) && (sanctoral[direktar_sanct]["den"]>=10) || (sanctoral[direktar_sanct]["mesic"]==4) || ((sanctoral[direktar_sanct]["mesic"]==5) && (sanctoral[direktar_sanct]["den"]<=10))) ? 1 : 0;
    var vzdy_advent = ((sanctoral[direktar_sanct]["mesic"]==12) && (sanctoral[direktar_sanct]["den"]>=3) && (sanctoral[direktar_sanct]["den"]<=24)) ? 1 : 0;
    var predvanocni = ((sanctoral[direktar_sanct]["mesic"]==12) && (sanctoral[direktar_sanct]["den"]>=17) && (sanctoral[direktar_sanct]["den"]<=24)) ? 1 : 0;

    if (online)
    {
      //document.write('<span> '+Barva(sanctoral[direktar_sanct]["barva"]));
      document.write('<a href="http://m.liturgie.cz/misal/'+sanctoral[direktar_sanct]["odkaz"]+'" class="kalendar_odkaz">');
      document.write('<span class="'+Barva(sanctoral[direktar_sanct]['barva'])+'">');
      if (sanctoral[direktar_sanct]["typ"])
      { 
        document.write(Typ(sanctoral[direktar_sanct]["typ"])+" ");
      }
      document.write(sanctoral[direktar_sanct]["nazev"]);  
      if (sanctoral[direktar_sanct]["podnazev"])
      {
        document.write(", "+sanctoral[direktar_sanct]["podnazev"]);
      }
      document.write("</span></a><br/>");
    }
    
    if (online_nedele)
    {
      return;
    }
    
    if (!online)
    {
      document.write("<b>Direktář</b>");
      document.write('<br/>');
    }
    document.write('Barva: '+BarvaSlovy(sanctoral[direktar_sanct]["barva"]));
    document.write('<br/>');
    
    if (sanctoral[direktar_sanct]["typ"])
    {
      if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write(', ');
      }
      if (online)
      {
        document.write('stupeň slavení: '+Typ(sanctoral[direktar_sanct]["typ"]).toLowerCase());
        //document.write(', ');
      }
      else
      {
        document.write('Stupeň slavení: '+Typ(sanctoral[direktar_sanct]["typ"]).toLowerCase());
        document.write('<br/>');
      }      
    }
    if (!online)
      {
        document.write('Pořadí v tabulce liturgických dnů: '+sanctoral[direktar_sanct]["tabulka_lit_dnu"]);
        document.write('<br/>');
      }
    if (!online)
    {
    if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>6)
    {
      document.write('Připadne-li na neděli: vynechává se');
    }
    else if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]==1)
    {
      document.write('Připadne-li na neděli: slaví se');
    }
    else if (vzdy_pust_nebo_velikonoce || vzdy_advent)
    {
      if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli: přesouvá se');} 
    }
    else if (pust)
    {
      if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na postní neděli: přesouvá se');}
    }
    else if (velikonoce)
    {
      if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na velikonoční neděli: přesouvá se');}
    }
    else if (advent)
    {
      if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na adventní neděli: přesouvá se');}
    }
    else
    {
      document.write('Připadne-li na neděli: slaví se');
    }    
    }
    document.write('<br/>');
    /*document.write('Připadne-li na neděli: ');
    if (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>6)
    {
      document.write('vynechává se<br/>');
    }
    else
    {
      document.write('slaví se<br/>');
    }*/
    
    if (sanctoral[direktar_sanct]["ke_msi"])
    {
      document.write('Ke mši: '+sanctoral[direktar_sanct]["ke_msi"]);
      if (pust && (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v době postní: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)</i>');
      }
      if (predvanocni && (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v předvánočním týdnu: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)</i>');
      }
    }
    if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }
    
    if (sanctoral[direktar_sanct]["k_liturgii_hodin"])
    {
      document.write('K liturgii hodin: '+sanctoral[direktar_sanct]["k_liturgii_hodin"]);
      if (pust && (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v době postní: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba o svatém; v ranních chválách a nešporách vše feriální, po závěrečné modlitbě ze dne se přidá antifona a modlitba o svatém)</i>');
      }
      if (predvanocni && (sanctoral[direktar_sanct]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v předvánočním týdnu: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba  o svatém; v ranních chválách a nešporách vše feriální, po závěrečné modlitbě ze dne se přidá antifona a modlitba o svatém)</i>');
      }
    }
    
    if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }

}

function DirektarVyjimky(direktar_vyjimky, online=false, online_nedele=false)
{
    var pust = ((vyjimky[direktar_vyjimky]["mesic"]==2) && (vyjimky[direktar_vyjimky]["den"]>=5) || (vyjimky[direktar_vyjimky]["mesic"]==3) || ((vyjimky[direktar_vyjimky]["mesic"]==4) && (vyjimky[direktar_vyjimky]["den"]<=24))) ? 1 : 0;
    var velikonoce = ((vyjimky[direktar_vyjimky]["mesic"]==3) && (vyjimky[direktar_vyjimky]["den"]>=22) || (vyjimky[direktar_vyjimky]["mesic"]==4) || (vyjimky[direktar_vyjimky]["mesic"]==5) || ((vyjimky[direktar_vyjimky]["mesic"]==6) && (vyjimky[direktar_vyjimky]["den"]<=13))) ? 1 : 0;
    var advent = ((vyjimky[direktar_vyjimky]["mesic"]==11) && (vyjimky[direktar_vyjimky]["den"]>=27) || ((vyjimky[direktar_vyjimky]["mesic"]==12) && (vyjimky[direktar_vyjimky]["den"]<=24))) ? 1 : 0;
    var vzdy_pust = ((vyjimky[direktar_vyjimky]["mesic"]==3) && (vyjimky[direktar_vyjimky]["den"]>=10) && (vyjimky[direktar_vyjimky]["den"]<=21)) ? 1 : 0;
    var vzdy_velikonoce = ((vyjimky[direktar_vyjimky]["mesic"]==4) && (vyjimky[direktar_vyjimky]["den"]>=25) || ((vyjimky[direktar_vyjimky]["mesic"]==5) && (vyjimky[direktar_vyjimky]["den"]<=10))) ? 1 : 0;
    var vzdy_pust_nebo_velikonoce = ((vyjimky[direktar_vyjimky]["mesic"]==3) && (vyjimky[direktar_vyjimky]["den"]>=10) || (vyjimky[direktar_vyjimky]["mesic"]==4) || ((vyjimky[direktar_vyjimky]["mesic"]==5) && (vyjimky[direktar_vyjimky]["den"]<=10))) ? 1 : 0;
    var vzdy_advent = ((vyjimky[direktar_vyjimky]["mesic"]==12) && (vyjimky[direktar_vyjimky]["den"]>=3) && (vyjimky[direktar_vyjimky]["den"]<=24)) ? 1 : 0;
    var predvanocni = ((vyjimky[direktar_vyjimky]["mesic"]==12) && (vyjimky[direktar_vyjimky]["den"]>=7) && (vyjimky[direktar_vyjimky]["den"]<=24)) ? 1 : 0;
	
	
    if (online)
    {
      document.write('<a href="http://m.liturgie.cz/misal/'+vyjimky[direktar_vyjimky]["odkaz"]+'" class="kalendar_odkaz">');
      document.write('<span class="'+Barva(vyjimky[direktar_vyjimky]['barva'])+'">');
      //document.write('<span> '+Barva(vyjimky[direktar_vyjimky]["barva"]));
      if (vyjimky[direktar_vyjimky]["typ"])
      { 
        document.write(Typ(vyjimky[direktar_vyjimky]["typ"])+" ");
      }
      document.write(vyjimky[direktar_vyjimky]["nazev"]);  
      if (vyjimky[direktar_vyjimky]["podnazev"])
      {
        document.write(", "+vyjimky[direktar_vyjimky]["podnazev"]);
      }
      document.write('<br/></span></a>');
    }

    if (online_nedele)
    {
      return;
    }

    if (!online)
    {
      document.write("<b>Direktář</b>");
      document.write('<br/>');
    }
     document.write('Barva: '+BarvaSlovy(vyjimky[direktar_vyjimky]["barva"]));
          if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write(', ');
      }

     
     
    if (vyjimky[direktar_vyjimky]["typ"])
    {
      if (online)
      {
        document.write('stupeň slavení: '+Typ(vyjimky[direktar_vyjimky]["typ"]).toLowerCase());
        //document.write(', ');
      }
      else
      {
        document.write('Stupeň slavení: '+Typ(vyjimky[direktar_vyjimky]["typ"]).toLowerCase());
        document.write('<br/>');
      }      
    }
    if (!online)
      {
        document.write('Pořadí v tabulce liturgických dnů: '+vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]);
        document.write('<br/>');
      }
    if (!online)
    {
    if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>6)
    {
      document.write('Připadne-li na neděli: vynechává se');
    }
    else if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==1)
    {
      document.write('Připadne-li na neděli: slaví se');
    }
    else if (vzdy_pust_nebo_velikonoce || vzdy_advent)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli: přesouvá se');} 
    }
    else if (pust)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na postní neděli: přesouvá se');}
    }
    else if (velikonoce)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na velikonoční neděli: přesouvá se');}
    }
    else if (advent)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se<br/>');} else {document.write('Připadne-li na neděli v mezidobí: slaví se<br/>Připadne-li na adventní neděli: přesouvá se');}
    }
    else
    {
      document.write('Připadne-li na neděli: slaví se');
    }    
    }
    document.write('<br/>');
    /*document.write('Připadne-li na neděli: ');
    if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>6)
    {
      document.write('vynechává se<br/>');
    }
    else
    {
      document.write('slaví se<br/>');
    }*/
    
    if (vyjimky[direktar_vyjimky]["ke_msi"])
    {
      document.write('Ke mši: '+vyjimky[direktar_vyjimky]["ke_msi"]);
      if (pust && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v době postní: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)</i>');
      }
      if (predvanocni && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v předvánočním týdnu: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)</i>');
      }
    }
    if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }
    
    if (vyjimky[direktar_vyjimky]["k_liturgii_hodin"])
    {
      document.write('K liturgii hodin: '+vyjimky[direktar_vyjimky]["k_liturgii_hodin"]);
      if (pust && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v době postní: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba  o svatém; v ranních chválách a nešporách vše feriální, místo závěrečné modlitby ze dne se přidá antifona a modlitba o svatém)</i>');
      }
      if (predvanocni && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<br/><i>– v předvánočním týdnu: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba  o svatém; v ranních chválách a nešporách vše feriální, místo závěrečné modlitby ze dne se přidá antifona a modlitba o svatém)</i>');
      }
    }
    
    if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }

    /*if (!online)
    {
      document.write("<b>Direktář</b>");
      document.write('<br/>');
    }
    if (vyjimky[direktar_vyjimky]["typ"])
    {
      document.write('Stupeň slavení: '+Typ(vyjimky[direktar_vyjimky]["typ"]));
      if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write(', ');
      }
    }
    document.write('Pořadí v tabulce liturgických dnů: '+vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]);
      if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write(', ');
      }

    if (vyjimky[direktar_vyjimky]["vynech_pripadne"]!=1)
    {
    if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>6)
    {
      document.write('Připadne-li na neděli: vynechává se');
    }
    else if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==1)
    {
      document.write('Připadne-li na neděli: slaví se');
    }
    else if (vzdy_pust_nebo_velikonoce || vzdy_advent)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se');} else {document.write('Připadne-li na neděli: přesouvá se');} 
    }
    else if (pust)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se');} else {document.write('Připadne-li na neděli v mezidobí: slaví sePřipadne-li na postní neděli: přesouvá se');}
    }
    else if (velikonoce)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se');} else {document.write('Připadne-li na neděli v mezidobí: slaví sePřipadne-li na velikonoční neděli: přesouvá se');}
    }
    else if (advent)
    {
      if (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]==5) {document.write('Připadne-li na neděli: vynechává se');} else {document.write('Připadne-li na neděli v mezidobí: slaví sePřipadne-li na adventní neděli: přesouvá se');}
    }
    else
    {
      document.write('Připadne-li na neděli: slaví se');
    }
    if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write(', ');
      }
    }
    
    document.write('Barva: '+BarvaSlovy(vyjimky[direktar_vyjimky]["barva"])+'<br/>');
    if (vyjimky[direktar_vyjimky]["ke_msi"])
    {
      document.write('Ke mši: '+vyjimky[direktar_vyjimky]["ke_msi"]+'<br/>');
      if (pust && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<i>– v době postní: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)<br/></i>');
      }
      if (predvanocni && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<i>– v předvánočním týdnu: lze slavit pouze pro připomínku (vstupní modlitba z památky, zbytek feriální)<br/></i>');
      }
    }
    if (vyjimky[direktar_vyjimky]["k_liturgii_hodin"])
    {
      document.write('K liturgii hodin: '+vyjimky[direktar_vyjimky]["k_liturgii_hodin"]+'<br/>');
      if (pust && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<i>– v době postní: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba  o svatém; v ranních chválách a nešporách vše feriální, místo závěrečné modlitby ze dne se přidá antifona a modlitba o svatém)<br/></i>');
      }
      if (predvanocni && (vyjimky[direktar_vyjimky]["tabulka_lit_dnu"]>9))
      {
        document.write('<i>– v předvánočním týdnu: lze slavit pouze pro připomínku (v modlitbě se čtením vše feriální, po čtení z Otců a responsoriu se připojí čtení o svatém s responsoriem a modlitba  o svatém; v ranních chválách a nešporách vše feriální, místo závěrečné modlitby ze dne se přidá antifona a modlitba o svatém)<br/></i>');
      }
    }
    */
}

function DirektarPravidelne(direktar_pravidelne, online=false, online_nedele=false)
{
    if (online)
    {
      document.write('<a href="http://m.liturgie.cz/misal/'+pravidelne[direktar_pravidelne]["odkaz"]+'" class="kalendar_odkaz">');
      document.write('<span class="'+Barva(pravidelne[direktar_pravidelne]['barva'])+'">');
      //document.write('<span> '+Barva(pravidelne[direktar_pravidelne]["barva"]));
      if (pravidelne[direktar_pravidelne]["typ"])
      { 
        document.write(Typ(pravidelne[direktar_pravidelne]["typ"])+" ");
      }
      document.write(pravidelne[direktar_pravidelne]["nazev"]);  
      /*if (pravidelne[direktar_pravidelne]["podnazev"])
      {
        document.write(", "+pravidelne[direktar_pravidelne]["podnazev"]);
      }*/
      document.write("</a><br/></span>");
    }

    if (online_nedele)
    {
      return;
    }

    if (!online)
    {
      document.write("<b>Direktář</b>");
      document.write('<br/>');
    }
    
    document.write('Barva: '+BarvaSlovy(pravidelne[direktar_pravidelne]["barva"])+'<br/>');
    if (!online)
      {
        document.write('Pořadí v tabulce liturgických dnů: '+pravidelne[direktar_pravidelne]["tabulka_lit_dnu"]);
        document.write('<br/>');
      }

    if (pravidelne[direktar_pravidelne]["ke_msi"])
    {
      document.write('Ke mši: '+pravidelne[direktar_pravidelne]["ke_msi"]);
    }
    if (pravidelne[direktar_pravidelne]["k_liturgii_hodin"])
    {
      if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }
      document.write('K liturgii hodin: '+pravidelne[direktar_pravidelne]["k_liturgii_hodin"]);
      if (online)
      {
        document.write('.<br/>');
      }
    }
    else if (!online)
      {
        document.write('<br/>');
      }
      else
      {
        document.write('. ');
      }
}

function NajdiPamatku()
{
  var url = JmenoURL();
  var sanct = 0;
  nalez_sanctoral = new Array();
  var nalez_sanctoral_i = 0; 
  while (sanct<sanctoral.length)
  {
    if (sanctoral[sanct]["odkaz"] == url)
    {
      nalez_sanctoral[nalez_sanctoral_i] = sanct;
      nalez_sanctoral_i++;
      //break;
    }
    sanct++;
  }
  var vyj = 0;
  nalez_vyjimky = new Array();
  var nalez_vyjimky_i = 0;
  while (vyj<vyjimky.length)
  {
    if (vyjimky[vyj]["odkaz"] == url)
    {
      nalez_vyjimky[nalez_vyjimky_i] = vyj;
      nalez_vyjimky_i++;
    }
    vyj++;
  }
  
  var prav = 0;
  nalez_pravidelne = new Array();
  var nalez_pravidelne_i = 0;
  while (prav<pravidelne.length)
  {
    if (pravidelne[prav]["odkaz"] == url)
    {
      nalez_pravidelne[nalez_pravidelne_i] = prav;
      nalez_pravidelne_i++;
    }
    prav++;
  }
    
  
  cteni = new Array();
  var ct = 0;
  var rok = zacatek_rok;
  var j=0;
  var k=0;
  var nalezeno_vyjimky;
  var pokracuj;
  var hlaska = "";
  while (rok <= konec_rok)
  {
        nalezeno_vyjimky = 0;
    pokracuj = 1;
    while ((j< nalez_vyjimky.length) && (pokracuj==1))
    {           
      if (vyjimky[nalez_vyjimky[j]]["datum"].getFullYear()>rok)
      {
	pokracuj = 0;
      }
      else
      {
	Rozd = RozdilDnu(vyjimky[nalez_vyjimky[j]]["datum"],zacatek);
	if (co_se_slavi[Rozd]["kde"]=="V")
	{
	  if ((vyjimky[nalez_vyjimky[j]]["typ"]=="P") || (vyjimky[nalez_vyjimky[j]]["typ"]=="PP") || (vyjimky[nalez_vyjimky[j]]["typ"]=="NP"))
	  {
	    cteni[ct] = new Array();
	    cteni[ct]["odkaz"] = "../"+pravidelne[Rozd]["odkaz"];
	    cteni[ct]["nazev"] = pravidelne[Rozd]["nazev"];
	    cteni[ct]["rok"] = rok;
	    ct++;
	  }
	  nalezeno_vyjimky = 1;
	  hlaska += VypisDatum(vyjimky[nalez_vyjimky[j]]["datum"])+ " – "+Typ(vyjimky[nalez_vyjimky[j]]["typ"])+" se slaví.<br/>";
	}
	else
	{
	  hlaska += VypisDatum(vyjimky[nalez_vyjimky[j]]["datum"])+ " – ";
	  if ((vyjimky[nalez_vyjimky[j]]["poradi"]=="PP") || (vyjimky[nalez_vyjimky[j]]["typ"]=="NP"))
	  {
	    if (co_se_slavi[Rozd]["kde"] == "P")
	    {
	      if (pravidelne[co_se_slavi[Rozd]["id"]]["tabulka_lit_dnu"] > 12)
	      {
		 hlaska += TentoTyp(vyjimky[nalez_vyjimky[j]]["typ"])+" se může slavit, anebo ";
	      }
	    }
	    else
	    if (co_se_slavi[Rozd]["kde"] == "S")
	    {
	      if (sanctoral[co_se_slavi[Rozd]["id"]]["tabulka_lit_dnu"] > 12)
	      {
		 hlaska += "Může se slavit toto, anebo ";
	      }
	    }
	    else
	    {
	      hlaska += "Neslaví se. Místo toho <br/>"; 
	    }
	  }
	  hlaska += VypisDen(Rozd)+"<br/>";
	}
	j++;        
      }      
    }
    
    if (nalezeno_vyjimky == 0)
    {
    for (i=0; i<nalez_sanctoral.length; i++)
    {
      Rozd = RozdilDnu(PrevedNaDatum(sanctoral[nalez_sanctoral[i]]["den"],sanctoral[nalez_sanctoral[i]]["mesic"],rok),zacatek);
      if ((Rozd>=0) && (Rozd<=celkem_dnu))
      {
	datum = PrevedNaDatum(sanctoral[nalez_sanctoral[i]]["den"],sanctoral[nalez_sanctoral[i]]["mesic"],rok);
	nezavazna = ((co_se_slavi[Rozd]["kde"]=="P") && (co_se_slavi[Rozd]["lze_slavit"] == "S") && (pravidelne[Rozd]["tabulka_lit_dnu"]>9));
	pripominka = (ZkontrolujProPripominku(datum)) && (pravidelne[Rozd]["tabulka_lit_dnu"]>=9) && (co_se_slavi[Rozd]["lze_slavit"] == "N");
	if ((co_se_slavi[Rozd]["kde"]=="S") || (nezavazna) || pripominka)
	{
	  if (((sanctoral[nalez_sanctoral[i]]["typ"]=="P") || (sanctoral[nalez_sanctoral[i]]["typ"]=="PP") || (sanctoral[nalez_sanctoral[i]]["typ"]=="NP")) && (!ZkontrolujProPripominku(co_se_slavi[Rozd]["datum"])))
	  {
	    cteni[ct] = new Array();
	    cteni[ct]["odkaz"] = "../"+pravidelne[Rozd]["odkaz"];
	    cteni[ct]["nazev"] = pravidelne[Rozd]["nazev"];
	    cteni[ct]["rok"] = rok;
	    ct++;
	  }
	  //document.write(ZkontrolujProPripominku(datum));
	  if (pripominka)
	  {
	    hlaska += VypisDatum(datum)+ " – "+Typ(sanctoral[nalez_sanctoral[0]]["typ"])+" se může slavit pro připomínku <i>(vstupní modlitba z památky, ostatní ze dne </i>"+VypisDen(Rozd)+"<i>)</i>.<br/>";
	  }
	  else if (nezavazna)
	  {
	    hlaska += VypisDatum(datum)+ " – "+Typ(sanctoral[nalez_sanctoral[0]]["typ"])+" se může slavit, anebo "+VypisDen(Rozd)+VypisSanctoralDatum(datum,sanctoral[nalez_sanctoral[i]]["nazev"])+".<br/>";
	  }
	  else
	  {
	    hlaska += VypisDatum(datum)+ " – "+Typ(sanctoral[nalez_sanctoral[0]]["typ"])+" se slaví.<br/>";
	  }
	  nalezeno_vyjimky = 1; // doplneno 2019-12-31 
	}
	/*else if (co_se_slavi[i]["lze_slavit"] == true)
	{
	  hlaska += VypisDatum(PrevedNaDatum(sanctoral[nalez_sanctoral[i]]["den"],sanctoral[nalez_sanctoral[i]]["mesic"],rok))+ " – "+Typ(sanctoral[nalez_sanctoral[0]]["typ"])+" se slaví.<br/>";
	}*/
	else
	{
	  hlaska += VypisDatum(PrevedNaDatum(sanctoral[nalez_sanctoral[i]]["den"],sanctoral[nalez_sanctoral[i]]["mesic"],rok)) + " – Neslaví se. Místo toho <br/>"+VypisDen(Rozd)+"<br/>";
	}
      }
    }
    }

    if (nalezeno_vyjimky == 0)
    {
    nalezeno_pravidelne = 0;
    pokracuj = 1;
    while ((k< nalez_pravidelne.length) && (pokracuj==1))
    {       
      if (co_se_slavi[nalez_pravidelne[k]]["datum"].getFullYear()<rok)
      {
        k++;
      }
      else if (co_se_slavi[nalez_pravidelne[k]]["datum"].getFullYear()>rok)
      {
	pokracuj = 0;
      }
      else
      {
	//Rozd = RozdilDnu(co_se_slavi[nalez_pravidelne[j]]["datum"],zacatek);
	Rozd = nalez_pravidelne[k];
	if (co_se_slavi[Rozd]["kde"]=="P")
	{
	  nalezeno_pravidelne = 1;
	  datum = co_se_slavi[Rozd]["datum"];
	  nezavazna = ((co_se_slavi[Rozd]["lze_slavit"] == "S") && (pravidelne[Rozd]["tabulka_lit_dnu"]>9));
	  pripominka = (ZkontrolujProPripominku(datum)) && (pravidelne[Rozd]["tabulka_lit_dnu"]>=9) && (co_se_slavi[Rozd]["lze_slavit"] == "N");
	  //document.write(ZkontrolujProPripominku(datum));
	  if (pripominka)
	  {
	    hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – Slaví se. Je ale také možné slavit pro připomínku "+VypisSanctoralDatum(datum)+" <i>(vstupní modlitba z památky, ostatní ze dne)</i>.<br/>";
	  }
	  else if (nezavazna)
	  {
	    //VypisSanctoralDatum(datum);
	    hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – Slaví se. Je ale také možné slavit nezávaznou památku "+VypisSanctoralDatum(datum)+".<br/>";
	  }
	  else
	  {
	    hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – Slaví se.<br/>";
	  }
	}
	else
	{
		//document.write(co_se_slavi[Rozd]["kde"]+pravidelne[Rozd]["tabulka_lit_dnu"]);
		/*if (!((co_se_slavi[Rozd]["kde"]=="S") && (pravidelne[Rozd]["tabulka_lit_dnu"] == sanctoral[nalez_sanctoral[i]]["tabulka_lit_dnu"])))
		{*/
			hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – Neslaví se. Místo toho <br/>"+VypisDen(Rozd)+"<br/>";
		//}
	}
	k++;        
      }      
    }

    /*for (i=0; i<nalez_pravidelne.length; i++)
    {
      //Rozd = RozdilDnu(co_se_slavi[nalez_pravidelne[i]]["datum"],zacatek);
      Rozd = nalez_pravidelne[i];
      if (Rozd>=0)
      {
	if (co_se_slavi[Rozd]["kde"]=="P")
	{
	  hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – slaví se.<br/>";
	}
	else
	{
	  hlaska += VypisDatum(co_se_slavi[Rozd]["datum"])+ " – Neslaví se. Místo toho "+VypisDen(Rozd)+".<br/>";
	}
      }
    }*/
    }
    rok++;
  }
  
  /*if (cteni.length >1)
  {
    hlaska = TentoTyp(sanctoral[nalez_sanctoral[0]]["typ"])+" se slaví v letech ";
    for (i=0; i<cteni.length; i++)
    {
      hlaska += cteni[i]["rok"]+", ";
    }   
  }*/
  document.write(hlaska);
  
  /*if (sanct<sanctoral.length)
  {
    rok = zacatek_rok;
    Rozd = RozdilDnu(PrevedNaDatum(sanctoral[sanct]["den"],sanctoral[sanct]["mesic"],rok),zacatek);
    while (Rozd <=celkem_dnu)
    {
      if (Rozd>=0)
      {
	if (co_se_slavi[Rozd]["kde"]=="S")
	{
	  if ((sanctoral[sanct]["typ"]=="P") || (sanctoral[sanct]["typ"]=="PP") || (sanctoral[sanct]["typ"]=="NP"))
	  {
	    cteni[ct] = new Array();
	    cteni[ct]["odkaz"] = "../"+pravidelne[Rozd]["odkaz"];
	    cteni[ct]["nazev"] = pravidelne[Rozd]["nazev"];
	    cteni[ct]["rok"] = rok;
	    ct++;
	  }
	  document.write(TentoTyp(sanctoral[sanct]["typ"])+" se v roce "+rok+" slaví.");
	}
      }
      rok++;
      Rozd = RozdilDnu(PrevedNaDatum(sanctoral[sanct]["den"],sanctoral[sanct]["mesic"],rok),zacatek);
    }
  }*/
  if (vyj<vyjimky.length)
  {
    //document.write(sanct+"xxx"+vyj+"xxx"+prav);
  }
  /*for (j=0; j<vypisovat.length; j++)
  {
    document.write(vypisovat["datum"]);
  }
  return; */
  
   document.write("<br/>");
  if (nalez_vyjimky.length>0)
  {
    DirektarVyjimky(nalez_vyjimky[0]);
  }
  else if (nalez_sanctoral.length>0)
  {
    DirektarSanktoral(nalez_sanctoral[0]);
  }
  else if (nalez_pravidelne.length>0)
  {
    DirektarPravidelne(nalez_pravidelne[0]);
  }
  if ((co_se_slavi[Rozd]["kde"]=="P") || ((co_se_slavi[Rozd]["kde"]=="S") && (sanctoral[nalez_sanctoral[0]]["tabulka_lit_dnu"]>=9)) || ((co_se_slavi[Rozd]["kde"]=="V") && (vyjimky[nalez_vyjimky[0]]["tabulka_lit_dnu"]>=9)))
  {
	  document.write("Žaltář "+pravidelne[Rozd]["zaltar"]+". týdne");
  }


}

function VypisCteni(presneji)
{
  var hlaska ="";
  var i=0;
  if (!presneji)
  {
    presneji = "1_cteni";
  }
  for (i=0; i<cteni.length; i++)
  {
    hlaska += "<a href=\""+cteni[i]["odkaz"]+"#"+presneji+"\"><p onClick=\"zobrazSkryj('oddil1')\"><button class=\"m\">Čtení ";
    if (cteni.length>1)
    {
      hlaska += "v roce "+cteni[i]["rok"]+" ";
    }
    hlaska += "("+cteni[i]["nazev"]+")</button></p></a>";
	    /*<p onClick="zobrazSkryj('oddil1')"><button>Čtení z Písma v roce A</button></p>
<div id="oddil1" class="skryvany"> \");*/  	  
  }
  hlaska += "<p onClick=\"zobrazSkryj('z_pamatky')\"><button>Přivlastněná a naznačená čtení</button></p>";
  document.write(hlaska);
}

function DirektarOnline()
{
  rozdil = RozdilDnu(dnes,zacatek);
  if ((rozdil>=0) && (rozdil<=celkem_dnu))
  {
    document.write("Dnešní den: ");
    if (co_se_slavi[rozdil]["kde"]=="S")
    {
      DirektarSanktoral(co_se_slavi[rozdil]["id"],true);
    }
    else if (co_se_slavi[rozdil]["kde"]=="P")
    {
      DirektarPravidelne(co_se_slavi[rozdil]["id"],true);
    }
    else if (co_se_slavi[rozdil]["kde"]=="V")
    {
      DirektarVyjimky(co_se_slavi[rozdil]["id"],true);
    }
    document.write("<br/>");
  }

  rozdil = rozdil +1;
  if ((rozdil>=0) && (rozdil<=celkem_dnu))
  {
    document.write("<br/>Zítřejší den: ");
    if (co_se_slavi[rozdil]["kde"]=="S")
    {
      DirektarSanktoral(co_se_slavi[rozdil]["id"],true);
    }
    else if (co_se_slavi[rozdil]["kde"]=="P")
    {
      DirektarPravidelne(co_se_slavi[rozdil]["id"],true);
    }
    else if (co_se_slavi[rozdil]["kde"]=="V")
    {
      DirektarVyjimky(co_se_slavi[rozdil]["id"],true);
    }
    document.write("<br/>");
  }

  rozdil = rozdil -1;
  var nedele = rozdil - co_se_slavi[rozdil]["datum"].getDay() + 7;  
  if ((nedele>=0) && (nedele<=celkem_dnu) && (nedele != rozdil+1))
  {
    document.write("<br/>Příští neděle: ");
    if (co_se_slavi[nedele]["kde"]=="S")
    {
      DirektarSanktoral(co_se_slavi[nedele]["id"],true,true);
    }
    else if (co_se_slavi[nedele]["kde"]=="P")
    {
      DirektarPravidelne(co_se_slavi[nedele]["id"],true,true);
    }
    else if (co_se_slavi[nedele]["kde"]=="V")
    {
      DirektarVyjimky(co_se_slavi[nedele]["id"],true,true);
    }
  }
}


function VypisMesiceSRokem()
{
	var akt = zacatek;
	var mesic = akt.getMonth();
	var rok = akt.getFullYear();
	var predchozi = "";
	document.write('<select name="rok_mesic">');
	while (RozdilDnu(akt,zacatek)  <= celkem_dnu)
	{
		predchozi += '<option value="'+rok+'_'+(DveCislice(mesic))+'" ';
		if ((akt.getMonth() == dnes.getMonth()) && (akt.getYear() == dnes.getYear()))
	    {
	    	predchozi += 'selected="selected"';
	    }
		predchozi += '>'+Mesic_1p[mesic]+' '+rok+'</option>';
		mesic++;
		if (mesic == 12)
		{
			mesic = 0;
			rok++;
		}
		akt = PrevedNaDatum(1,mesic+1,rok);
	}
	document.write(predchozi);	
	document.write('</select>');
}

function OdkazNaDen()
{
	var a_odkaz = document.getElementById('OdkazNaDen');
	var nalezeno = document.getElementById('nalezeno');
	if (den_moznosti.den.value == "dnes")
	{
		var rozdil = RozdilDnu(dnes,zacatek);
	}
	else if (den_moznosti.den.value == "zítra")
	{
		var rozdil = RozdilDnu(dnes,zacatek)+1;
	}
	else if (den_moznosti.den.value == "neděle")
	{
		var rozdil = RozdilDnu(dnes,zacatek);
	    rozdil = rozdil - dnes.getDay() + 7;
	}
	else
	{
		var str = zobraz_den.rok_mesic.value;
		var rok = Number(str.substr(0,str.indexOf("_")));
		var mesic = Number(str.substr(str.indexOf("_")+1));
		var rozdil = RozdilDnu(PrevedNaDatum(Number(zobraz_den.den.value), mesic+1, rok),zacatek);
	}
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		a_odkaz.href='http://m.liturgie.cz/misal/'+vypisovat["odkaz"];
		nalezeno.style.display = "block";
		nenalezeno.style.display = "none";
	}
	else
	{
		nalezeno.style.display = "none";
		nenalezeno.style.display = "block";
	}
	//document.links('odkaz').href='http://www.google.cz';
}

function TlacitkaJakoForm()
{
	var rozdil = RozdilDnu(dnes,zacatek);
	var nedele = rozdil - dnes.getDay() + 7;
    document.write('<form name="den_moznosti" method="post" onsubmit="return;" onChange="OdkazNaDen();">');
	if (!((nedele < 0) || (rozdil > celkem_dnu)))
	{
	//document.write('<table><tr><td align="left" width="142px" rowspan="2">');
	if ((rozdil>=0) && (rozdil<=celkem_dnu))
	{
		AktualniVypisovat(rozdil);
		document.write('<input type="radio" name="den" value="dnes" checked>Dnes</input><br/>');
		 
	}
	//document.write('</td><td align="right" width="142px">');
	if ((rozdil>=-1) && (rozdil<celkem_dnu))
	{
		document.write('<input type="radio" name="den" value="zítra">Zítra</input><br/>');
	}
	//document.write('</td></tr><tr><td height="32px">');
	if ((nedele>=0) && (nedele<=celkem_dnu) && (dnes.getDay()!=6))
	{
		document.write('<input type="radio" name="den" value="neděle">Neděle</input><br/>');
	}
	document.write('<input type="radio" name="den" value="datum">Přejdi na datum </input>');
	//document.write('</td></tr></table>');
	}
	
	
	document.write('</form><form name="zobraz_den" method="post" onsubmit="return;" onChange="OdkazNaDen();">');
    document.write('<select name="den">');
	for (i=1; i<32; i++)
	{
		document.write('<option value="'+i+'" ');
		if (i == dnes.getDate())
	    {
	    	document.write('selected="selected"');
	    }
		document.write('>'+i+'</option>');
    }
	document.write('</select>');
	VypisMesiceSRokem();
	//document.write('<input type="submit">');
	document.write('</form>');
	document.write('<div id="nalezeno">');
    document.write('<a href="http://m.liturgie.cz/misal/'+vypisovat['odkaz']+'" id="OdkazNaDen" style="text-decoration: none;" target="_blank"><input type="button" value="Ukaž"></a>');
	document.write('</div>');
	document.write('<div id="nenalezeno" style="display: none">');
    document.write('Aplikace je k dispozici na aktuální liturgický rok (od '+VypisDatum(zacatek)+' do '+VypisDatum(konec)+').');
	document.write('</div>');
}
