rep='<div style="padding: 10px; background-color: lightcoral; text-align: center; font-size: 20px; -webkit-text-stroke: thin; text-transform: uppercase;">Pracovní kopie sloužící ke gramatické a typografické analýze textu<\/div>'
find m.liturgie.cz/misalv2/ -name '*.htm*' | xargs sed -i "/<body/{N; /\n${rep}\$/b; s/\n/\n${rep}\n/}"

for i in $(jq -r '.| keys_unsorted[] | select(contains("jazyk") | not)' data.json)
do
  filev2="m.liturgie.cz/misalv2/${i#*/misal/}"
  echo "${i} ==> ${filev2}"
  for word in $(jq -r ".\"${i}\"[]" data.json | sort -u)
  do
    sed -ri "s/\<($word)\>/<mark>\1<\/mark>/g" $filev2
  done
  #'(.*misal)(.*)'
done
